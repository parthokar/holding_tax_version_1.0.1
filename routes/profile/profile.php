<?php

use App\Http\Controllers\UpdateProfileController;

Route::get('update/profile',[UpdateProfileController::class, 'showUpdateProfileForm'])->name('user.profile');

Route::post('update/profile/{user_id}/{user_type}', [UpdateProfileController::class, 'updateUserProfile'])->name('update.profile');
