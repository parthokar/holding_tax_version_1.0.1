<?php 

use App\Http\Controllers\DashboardController;

Route::middleware('check-permission:super|dc|uno|pouroshova|union|pouroshova-accessor|union-accessor|pouroshova-tax-collector|union-tax-collector|tax-payer')->group(function () {
  
   Route::get('dashboard', [DashboardController::class,'dashboard'])->name('user.dashboard');

});


