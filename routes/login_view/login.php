<?php

use App\Http\Controllers\UserAccessController;

Route::get('/super/admin', [UserAccessController::class, 'superView']);
Route::get('/dc/admin', [UserAccessController::class, 'dcView']);
Route::get('/uno/admin', [UserAccessController::class, 'unoView']);
Route::get('/pouroshova/admin', [UserAccessController::class, 'pouroshovaView']);
Route::get('/union/admin', [UserAccessController::class, 'unionView']);
Route::get('/pouroshova/accessor/admin', [UserAccessController::class, 'paccessorView']);
Route::get('/union/accessor/admin', [UserAccessController::class, 'uaccessorView']);
Route::get('/pouroshova/tax/collector/admin', [UserAccessController::class, 'ptaxcollectorView']);
Route::get('/union/tax/collector/admin', [UserAccessController::class, 'utaxcollectorView']);

