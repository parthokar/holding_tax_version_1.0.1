<?php

use App\Http\Controllers\GlobalControllers\GlobalController;
use Illuminate\Support\Facades\Route;

Route::get('get-upazila-by-district-id/{id}',[GlobalController::class, 'getUpazilasByDistrictId'])->name(('get-upazila-by-district-id'));
