<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Super\Auth\LoginController;


// cash clear route
Route::get('/cc', function () {
       Artisan::call('cache:clear');
       Artisan::call('route:clear');
       Artisan::call('view:clear');
       return '<h1>Cache, Route, View cache cleared.</h1>';
});
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login')->middleware('guest:web');
//all route file are included
require('website.php');
require('login_view/login.php');
require('login/login.php');
require('dashboard/dashboard.php');
require('admin/super.php');
require('admin/dc.php');
require('admin/pouroshova.php');
require('admin/pouroshova_accessor.php');
require('admin/pouroshova_tax_collector.php');
require('admin/union.php');
require('admin/union_accessor.php');
require('admin/union_tax_collector.php');
require('admin/uno.php');
require('global/global.php');
require('profile/profile.php');






