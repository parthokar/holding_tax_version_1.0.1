<?php 

use App\Http\Controllers\UserAccessController;


Route::post('/user/login', [UserAccessController::class, 'login'])->name('user.login');

Route::get('/user/logout', [UserAccessController::class, 'userLogout'])->name('user.logout');

