<?php

use App\Http\Controllers\pouroshova\AccessorController;
use App\Http\Controllers\pouroshova\PouroshovaController;


Route::middleware('check-permission:super|pouroshova')->group(function(){

    //pouroshova
    Route::resource('/pouroshova',PouroshovaController::class);

    Route::get('accessor/create', [AccessorController::class,'createAccessorForm'])->name('accessor.create');

});

