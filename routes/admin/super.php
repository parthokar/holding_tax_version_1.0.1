<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Super\DCController;
use App\Http\Controllers\Super\DistrictController;
use App\Http\Controllers\Super\Auth\LoginController;
use App\Http\Controllers\Super\DesignationController;
use App\Http\Controllers\Super\UnionController;
use App\Http\Controllers\Super\UpazilaController;
use App\Http\Controllers\Super\UserController;
use App\Http\Controllers\pouroshova\PouroshovaController;
use App\Http\Controllers\porishod\UnionPorishodController;
use App\Http\Controllers\Super\UnoController;
use App\Http\Controllers\word\WordController;
use App\Http\Controllers\user_store\UserStoreController;
use App\Http\Controllers\ajax\AjaxController;
use App\Http\Controllers\SmsSettingsController;
use App\Http\Controllers\sms\SmsController;

Route::middleware('check-permission:super')->group(function(){
    //Designation Routes
    Route::resource('designation', DesignationController::class)->except(['show', 'create','destroy']);
    Route::get('get-all-designation', [DesignationController::class, 'getAllDesignation'])->name('get-all-designation');
    //District Routes
    Route::resource('district', DistrictController::class)->except(['show', 'create','destroy']);
    Route::get('get-all-district', [DistrictController::class, 'getAllDistrict'])->name('get-all-district');
    //Dc Routes
    Route::resource('dc', DCController::class)->except(['show', 'destroy']);
    //Upazila'r Routes
    Route::resource('upazila', UpazilaController::class)->except(['show', 'create', 'destroy']);
    Route::get('get-all-upazila', [UpazilaController::class, 'getAllUpazila'])->name('get-all-upazila');
    Route::resource('union', UnionController::class)->except(['show', 'create', 'destroy']);
    Route::get('get-all-union', [UnionController::class, 'getAllUnion'])->name('get-all-union');
    Route::get('create/user',[UserController::class,'createUser'])->name('create.user');
    Route::resource('/pouroshova',PouroshovaController::class);
    Route::resource('union_porishod',UnionPorishodController::class);
    Route::resource('words',WordController::class);

    //user store route
    Route::get('/user/form/',[UserStoreController::class,'formChange'])->name('user.form');

   //zilla wise upazilla ajax
    Route::get('user/zilla/{id}',[AjaxController::class,'upazilla']);

   //zilla,upazilla wise union ajax
    Route::get('user/zilla/upazilla/union/{zid}/{uid}',[AjaxController::class,'union']);

    Route::resource('sms',SmsSettingsController::class);

    //filter wise sms send start
    Route::get('all/sms',[SmsController::class,'allSms'])->name('all.sms');
    Route::post('all/sms/send/general',[SmsController::class,'allGeneral'])->name('all.general');
    Route::post('all/sms/send/business',[SmsController::class,'allBusiness'])->name('all.business');
    Route::post('all/business/general',[SmsController::class,'allBusinessGeneral'])->name('all.business.general');
    Route::get('download/mobile',[SmsController::class,'downloadMobile'])->name('download.mobile');
    Route::post('download/mobile/union',[SmsController::class,'downloadMobileUnion'])->name('download.mobile.union');
    Route::post('download/mobile/word',[SmsController::class,'downloadMobileWord'])->name('download.mobile.word');
   //filter wise sms send end
});
   
 Route::middleware('check-permission:super|dc|uno|pouroshova|union|pouroshova-accessor|union-accessor|pouroshova-tax-collector|union-tax-collector|tax-payer')->group(function () {
    Route::post('/user/store/{type}', [UserStoreController::class, 'userStore'])->name('user.store');
 });
 


