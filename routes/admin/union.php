<?php

use App\Http\Controllers\house\HouseController;
use App\Http\Controllers\room\RoomController;
use App\Http\Controllers\job\JobController;
use App\Http\Controllers\tax\TaxSetupController;




Route::middleware('check-permission:union')->group(function(){
    
    //house setup
    Route::resource('house',HouseController::class);

   //room setup
    Route::resource('room',RoomController::class);

   //job setup
    Route::resource('job',JobController::class);

    //tax setup
    Route::resource('taxsetup',TaxSetupController::class);

});

