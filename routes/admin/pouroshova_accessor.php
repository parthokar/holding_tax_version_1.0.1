<?php

use App\Http\Controllers\pouroshova_accessor\TaxPayerController;


Route::middleware('check-permission:pouroshova-accessor')->group(function () {
    Route::get('create/tax-payer',[TaxPayerController::class, 'showTaxPayerCreateForm'])->name('taxPayer.create');
    Route::get('/tax-payer/form/', [TaxPayerController::class, 'formChange'])->name('tax-payer.form');
    Route::post('/tax-payer/form/', [TaxPayerController::class, 'storeGeneralHoldingTax'])->name('tax-payer.form');
    Route::post('general-holding-tax', [TaxPayerController::class, 'getGeneralHoldingTax'])->name('general-holding-tax');
});

