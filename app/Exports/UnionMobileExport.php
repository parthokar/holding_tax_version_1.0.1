<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class UnionMobileExport
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('backend.sms.sms_send.excel.union_excel', [
            'data' => $this->data
        ]);
    }
 

}
