<?php


namespace App\Traits;

use Illuminate\Support\Facades\Hash;

use App\Models\User;

use App\Models\DcAdmin;
use App\Models\UnoAdmin;
use App\Models\PouroshovaAdmin;
use App\Models\UnionAdmin;
use App\Models\PouroshovaAccessor;
use App\Models\UnionAccessor;
use App\Models\PouroshovaTaxCollector;
use App\Models\UnionTaxCollector;
use App\Notifications\UserEmailPassNotification;
use session;


trait UserStoreTrait{

  //this is for send sms
   public function sendSms($mobile,$message){
        $url = "http://premium.mdlsms.com/smsapi";
        $data=[
          "api_key" => "C2000808603de7bf6e9249.14298062",
          "type" => "text",
          "contacts" => $mobile,
          "senderid" => "8809612440735",
          "msg" => $message,
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
   }

   //send sms multiple
  public function sendSmsMultiple($mobile,$message) {
    $url = "http://premium.mdlsms.com/smsapimany";
    $data = [
      "api_key" => "C2000808603de7bf6e9249.14298062",
      "senderid" => "8809612440735",
      "messages" => json_encode( [
        [
          "to" => $mobile,
          "message" => $message
        ],
      ])
    ];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    curl_close($ch);
  }

    public function storeUser($request,$userType){
       //this is for dc admin user
       if($userType==2){
         $user=new User;
         $user->first_name=$request->first_name;
         $user->last_name=$request->last_name;
         $user->email=$request->email;
         $user->password=Hash::make($request['password']);
         $user->user_type=2;
         $user->status=1;
         $user->save();
         $dcUser=new DcAdmin;
         $dcUser->zilla_id=$request->zilla_id;
         $dcUser->designation_id=$request->designation_id;
         $dcUser->mobile_no=$request->mobile_no;
         $dcUser->user_id=$user->id;
         $dcUser->nid =$request->nid;
         $dcUser->user_id=$user->id;
         $dcUser->save();
        //  /*
        //  This notification for email and password change
        //  $user->notify( new UserEmailPassNotification($user,$request->password));
        // //  */

        //sms
        $url=url('/dc/admin');
        $mobile=$request->mobile_no;
        $message="Congratulation,Dear Sir Holding Tax Software System Registration Successfully. Your Login User ID. $request->email  Password $request->password . login link .$url. Powered By UP Automation Assessment Pvt. Ltd. Help Line 01619214125";
        $this->sendSms($mobile,$message);
        return redirect()->route('create.user')->with('success', 'User has been created successfully')->send();
       }

       //this is for uno admin
       if($userType==3){
        $user=new User;
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->email=$request->email;
        $user->password=Hash::make($request['password']);
        $user->user_type=3;
        $user->status=1;
        $user->save();
        //uno user data
        $unoUser=new UnoAdmin;
        $unoUser->zilla_id=$request->zilla_id;
        $unoUser->upazilla_id=1;
        $unoUser->designation_id=$request->designation_id;
        $unoUser->mobile_no=$request->mobile_no;
        $unoUser->user_id=$user->id;
        $unoUser->post_code=$request->post_code;
        $unoUser->nid =$request->nid;
        $unoUser->save();

        //sms 
        $url=url('/uno/admin');
        $mobile=$request->mobile_no;
        $message="Dear Sir Your Upozila Admin Registration Successfully.Your User ID. $request->email  Password $request->password . Admin Login Link .$url. Powered By UP Automation Assessment Pvt. Ltd. Help Line 01619214125";
        $this->sendSms($mobile,$message);
        return redirect()->route('create.user')->with('success', 'User has been created successfully')->send();
      }

       //this is for pouroshova admin
       if($userType==4){
        $user=new User;
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->email=$request->email;
        $user->password=Hash::make($request['password']);
        $user->user_type=4;
        $user->status=1;
        $user->save();
        //pouroshova admin user data
        $pouroshovaUser=new PouroshovaAdmin;
        $pouroshovaUser->zilla_id=$request->zilla_id;
        $pouroshovaUser->upazilla_id=$request->upazilla_id;
        $pouroshovaUser->pouroshova_id=$request->pouroshova_id;
        $pouroshovaUser->designation_id=$request->designation_id;
        $pouroshovaUser->mobile_no=$request->mobile_no;
        $pouroshovaUser->user_id=$user->id;
        $pouroshovaUser->post_code=$request->post_code;
        $pouroshovaUser->expired_date = $request->expired_date;
        $pouroshovaUser->charge_type = $request->charge_type;
        $pouroshovaUser->software_charge = $request->software_charge;
        $pouroshovaUser->charge = $request->charge;
        $pouroshovaUser->renew_charge = $request->renew_charge;
        $pouroshovaUser->nid = $request->nid;
        $pouroshovaUser->save();

        //sms 
        $url=url('/pouroshova/admin');
        $mobile=$request->mobile_no;
        $message="Dear Sir Your pouroshova Admin Registration Successfully.Your User ID. $request->email  Password $request->password . Admin Login Link .$url. Powered By UP Automation Assessment Pvt. Ltd. Help Line 01619214125";
        $this->sendSms($mobile,$message);
        return redirect()->route('create.user')->with('success', 'User has been created successfully')->send();
      }

      //this is for union admin
      if($userType==5){
      $user=new User;
      $user->first_name=$request->first_name;
      $user->last_name=$request->last_name;
      $user->email=$request->email;
      $user->password=Hash::make($request['password']);
      $user->user_type=5;
      $user->status=1;
      $user->save();
      //union admin user data
      $pouroshovaUser=new UnionAdmin;
      $pouroshovaUser->zilla_id=$request->zilla_id;
      $pouroshovaUser->upazilla_id=$request->upazilla_id;
      $pouroshovaUser->up_id=$request->up_id;
      $pouroshovaUser->designation_id=$request->designation_id;
      $pouroshovaUser->mobile_no=$request->mobile_no;
      $pouroshovaUser->user_id=$user->id;
      $pouroshovaUser->post_code=$request->post_code;
      $pouroshovaUser->expired_date = $request->expired_date;
      $pouroshovaUser->charge_type = $request->charge_type;
      $pouroshovaUser->charge = $request->charge;
      $pouroshovaUser->renew_charge = $request->renew_charge;
      $pouroshovaUser->nid = $request->nid;
      $pouroshovaUser->save();
      //sms 
      $url=url('/union/admin');
      $mobile=$request->mobile_no;
      $message="Dear Sir Your union Admin Registration Successfully.Your User ID. $request->email  Password $request->password . Admin Login Link .$url. Powered By UP Automation Assessment Pvt. Ltd. Help Line 01619214125";
      $this->sendSms($mobile,$message);
      return redirect()->route('create.user')->with('success', 'User has been created successfully')->send();
    }


    //this is for pouroshova accessor
    if($userType==6){
      $user=new User;
      $user->first_name=$request->first_name;
      $user->last_name=$request->last_name;
      $user->email=$request->email;
      $user->password=Hash::make($request['password']);
      $user->user_type=6;
      $user->status=1;
      $user->save();
      //pouroshova accessor user data
      $pouroshovaAccessor=new PouroshovaAccessor;
      $pouroshovaAccessor->zilla_id=$request->zilla_id;
      $pouroshovaAccessor->pouroshova_id=$request->pouroshova_id;
      $pouroshovaAccessor->word_id=$request->word_id;
      $pouroshovaAccessor->designation_id=$request->designation_id;
      $pouroshovaAccessor->mobile_no=$request->mobile_no;
      $pouroshovaAccessor->user_id=$user->id;
      $pouroshovaAccessor->post_code=$request->post_code;
      $pouroshovaAccessor->save();
      //sms 
      $url=url('/pouroshova/accessor/admin');
      $mobile=$request->mobile_no;
      $message="Dear Assessor Your Login User ID. $request->email  Password $request->password .Login Link .$url. Powered By UP Automation Assessment Pvt. Ltd. Help Line 01619214125";
      $this->sendSms($mobile,$message);
      return redirect()->route('accessor.create')->with('success', 'User has been created successfully')->send();
    }

    //this is for union accessor
    if($userType==7){
      $user=new User;
      $user->first_name=$request->first_name;
      $user->last_name=$request->last_name;
      $user->email=$request->email;
      $user->password=Hash::make($request['password']);
      $user->user_type=7;
      $user->status=1;
      $user->save();
      //union accessor user data
      $unionAccessor=new UnionAccessor;
      $unionAccessor->zilla_id=$request->zilla_id;
      $unionAccessor->upazilla_id=$request->upazilla_id;
      $unionAccessor->up_id=$request->up_id;
      $unionAccessor->word_id=$request->word_id;
      $unionAccessor->designation_id=$request->designation_id;
      $unionAccessor->mobile_no=$request->mobile_no;
      $unionAccessor->user_id=$user->id;
      $unionAccessor->post_code=$request->post_code;
      $unionAccessor->save();
      //sms 
      $url=url('/union/accessor/admin');
      $mobile=$request->mobile_no;
      $message="Dear Assessor Your Login User ID. $request->email  Password $request->password .Login Link .$url. Powered By UP Automation Assessment Pvt. Ltd. Help Line 01619214125";
      $this->sendSms($mobile,$message);
      return redirect()->route('create.user')->with('success', 'User has been created successfully')->send();
   }

   //this is for pouroshova tax collector
   if($userType==8){
      $user=new User;
      $user->first_name=$request->first_name;
      $user->last_name=$request->last_name;
      $user->email=$request->email;
      $user->password=Hash::make($request['password']);
      $user->user_type=8;
      $user->status=1;
      $user->save();
      //pouroshova tax collector user data
      $unionAccessor=new PouroshovaTaxCollector;
      $unionAccessor->zilla_id=$request->zilla_id;
      $unionAccessor->pouroshova_id=$request->pouroshova_id;
      $unionAccessor->word_id=$request->word_id;
      $unionAccessor->designation_id=$request->designation_id;
      $unionAccessor->mobile_no=$request->mobile_no;
      $unionAccessor->user_id=$user->id;
      $unionAccessor->post_code=$request->post_code;
      $unionAccessor->save();
      //sms 
      $url=url('/pouroshova/tax/collector/admin');
      $mobile=$request->mobile_no;
      $message="Dear Tax Collector. Your Login User ID. $request->email  Password $request->password .Login Link .$url. Powered By UP Automation Assessment Pvt. Ltd. Help Line 01619214125";
      $this->sendSms($mobile,$message);
      return redirect()->route('create.user')->with('success', 'User has been created successfully')->send();
  }

   //this is for union tax collector
   if($userType==9){
      $user=new User;
      $user->first_name=$request->first_name;
      $user->last_name=$request->last_name;
      $user->email=$request->email;
      $user->password=Hash::make($request['password']);
      $user->user_type=9;
      $user->status=1;
      $user->save();
      //union tax collector user data
      $unionAccessor=new UnionTaxCollector;
      $unionAccessor->zilla_id=$request->zilla_id;
      $unionAccessor->upazilla_id=$request->upazilla_id;
      $unionAccessor->union_id=$request->union_id;
      $unionAccessor->word_id=$request->word_id;
      $unionAccessor->designation_id=$request->designation_id;
      $unionAccessor->mobile_no=$request->mobile_no;
      $unionAccessor->user_id=$user->id;
      $unionAccessor->post_code=$request->post_code;
      $unionAccessor->save();
      //sms 
      $url=url('/pouroshova/tax/collector/admin');
      $mobile=$request->mobile_no;
      $message="Dear Tax Collector. Your Login User ID. $request->email  Password $request->password .Login Link .$url. Powered By UP Automation Assessment Pvt. Ltd. Help Line 01619214125";
      $this->sendSms($mobile,$message);
      return redirect()->route('create.user')->with('success', 'User has been created successfully')->send();
   }

 }

}
