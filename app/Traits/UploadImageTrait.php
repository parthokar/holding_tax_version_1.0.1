<?php
namespace App\Traits;

trait UploadImageTrait{

    public function userImageUpload($image,$path,$field,$user) :bool{
        $ext = $image->getClientOriginalExtension();
        $name =  hexdec(uniqid()) . '.' . $ext;
        $last_image = $path . $name;
        $user->$field = $last_image;
        if($user->save()){
            $image->move($path, $last_image);
        }
        return true;
    }
}
