<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaxSetupValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

   
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'house_id' => 'required',
            'room_id' => 'required',
            'job_id' => 'required',
            'tax_amount' => 'required'
        ];
    }

     /**
     * Get the validation custom message that apply to the view.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'house_id.required'    => 'দয়া করে বাড়ি নির্বাচন করুন',
            'room_id.required'     => 'দয়া করে রুম নির্বাচন করুন',
            'job_id.required'      => 'দয়া করে পেশা নির্বাচন করুন',
            'tax_amount.required'  => 'দয়া করে করের পরিমাণ প্রবেশ করান',
        ];
    }
}
