<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HouseValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'house_name' => 'required|max:255'
        ];
    }

     /**
     * Get the validation custom message that apply to the view.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'house_name.required'      => 'দয়া করে বাড়ির নাম লিখুন',
        ];
    }
}
