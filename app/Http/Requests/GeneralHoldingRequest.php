<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralHoldingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tax_way' => ['required'],
            'ward_id' => ['required'],
            'holding_no' => ['required'],
            'village' => ['required'],
            'post_name' => ['required'],
            'post_code' => ['required'],
            'religion' => ['required'],
            'gender' => ['required'],
            'name' => ['required'],
            'nid' => ['required'],
            'home_no' => ['required'],
            'home_conditions' => ['required'],
            'room_no' => ['required'],
            'job' => ['required'],
            'mobile' => ['required'],
            'email' => ['required'],
            'vumir_poriman' => ['required'],
            'vumir_poriman_shotok' => ['required'],
            'abadi_poriman' => ['required'],
            'sanitation' => ['required'],
            'sompoti' => ['required'],
            'family_member' => ['required'],
            'male' => ['required'],
            'female' => ['required'],
            'has_tubewell' => ['required'],
            'has_electricty' => ['required'],
            'tax_value' => ['required'],
            'last_tax_year' => ['required'],
            'tax_per_year' => ['required'],
        ];
    }
}
