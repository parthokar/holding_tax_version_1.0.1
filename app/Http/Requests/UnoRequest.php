<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'district_id' => 'required',
            'upazila_id' => 'required',
            'designation_id' => 'required',
            'first_name' => 'required | string',
            'last_name' => 'required | string',
            'email' => 'required|email|unique:dcs',
            'mobile' => 'required|numeric',
            'password' => 'required|min:3',
            'post_code' => 'required|numeric',
        ];
    }
}
