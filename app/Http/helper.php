<?php

use App\Models\Dc;
use App\Models\DcAdmin;
use App\Models\Uno;
use App\Models\User;
use App\Models\UnionAdmin;
use App\Models\UnionAccessor;
use App\Models\PouroshovaAdmin;
use App\Models\UnionTaxCollector;
use App\Models\PouroshovaAccessor;
use App\Models\PouroshovaTaxCollector;
use App\Models\UnoAdmin;
use Rakibhstu\Banglanumber\NumberToBangla;

function checkPermission($permissions){
    $userAccess = getMyPermission(Auth::user() && auth()->user()->user_type);
    foreach ($permissions as $key => $value) {
        if($value == $userAccess){
            return true;
        }
    }
    return false;
}

function getMyPermission($id)
{
    switch ($id) {
        case 1:
            return 'super';
            break;
        case 2:
            return 'dc';
            break;
        case 3:
            return 'uno';
            break;
        case 4:
            return 'pouroshova';
            break;
        case 5:
            return 'union';
            break;
        case 6:
            return 'pouroshova-accessor';
            break;
        case 7:
            return 'union-accessor';
            break;
        case 8:
            return 'pouroshova-tax-collector';
            break;
        case 9:
            return 'union-tax-collector';
            break;
        case 10:
            return 'tax-payer';
            break;
    }
}
//get user data for edit
if (!function_exists('getUpdateUserData')) {
    function getUpdateUserData()
    {
        $type = auth()->user()->user_type;
        $id = auth()->id();
        if ($type == 1) {
            return User::where('id', $id)->first();
        }
        if ($type == 2) {
            return DcAdmin::where('user_id', $id)->first();
        }
        if ($type == 3) {
            return UnoAdmin::where('user_id', $id)->first();
        }
        if ($type == 4) {
            return PouroshovaAdmin::where('user_id', $id)->first();
        }
        if ($type == 5) {
            return UnionAdmin::where('user_id', $id)->first();
        }
        if ($type == 6) {
            return PouroshovaAccessor::where('user_id', $id)->first();
        }
        if ($type == 7) {
            return UnionAccessor::where('user_id', $id)->first();
        }
        if ($type == 8) {
            return PouroshovaTaxCollector::where('user_id', $id)->first();
        }
        if ($type == 9) {
            return UnionTaxCollector::where('user_id', $id)->first();
        }
    }
}

//this function receive english number and return bangla number

function englishToBanglaNumber($num){
    $n = new NumberToBangla();
    return $n->bnNum($num);
}
