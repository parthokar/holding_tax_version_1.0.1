<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SmsSettings;

class SmsSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=SmsSettings::all();

        $this->data['main_menu'] = 'SMS_SETUP';

        $this->data['sub_menu'] = 'SMS_SETUP';

        return view('backend.sms.index',$this->data,compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list=SmsSettings::all();

        $edit=SmsSettings::find($id);

        $this->data['main_menu'] = 'SMS_SETUP';

        $this->data['sub_menu'] = 'SMS_SETUP';

        return view('backend.sms.edit',$this->data,compact('list','edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit=SmsSettings::find($id);

        $edit->status=$request->status;

        $edit->save();

        return redirect()->back()->with('success', 'সফলভাবে আপডেট করা হয়েছে');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
