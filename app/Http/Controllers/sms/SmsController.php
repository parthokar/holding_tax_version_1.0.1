<?php

namespace App\Http\Controllers\sms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UnionPorishod;
use App\Models\Word;
use App\Models\UnionAdmin;
use App\Models\GeneralHolding;
use App\Models\PouroshovaAccessor;
use App\Exports\UnionMobileExport;
use App\Exports\WordMobileExport;
use App\Traits\UserStoreTrait;
use Excel;


class SmsController extends Controller
{
    use UserStoreTrait;

    //all sms send view
    public function allSms(){
        $this->data['main_menu'] = 'SMS_SETUP';
        $this->data['sub_menu'] = 'SMS_SETUP';
        return view('backend.sms.sms_send.all_sms',$this->data);
    }

    //send sms allGeneral holding
    public function allGeneral(Request $request){
        $data=GeneralHolding::select('mobile')->get();
        foreach($data as $user){
            $mobile=$user->mobile;
            $message=$request->message;
            $this->sendSmsMultiple($mobile,$message);
        }
        return redirect()->route('all.sms')->with('success', 'Message has been send successfully');
    }

    //send sms allBusiness holding
    public function allBusiness(Request $request){
        dd('coming soon');
    }

    //business general all sms send
    public function allBusinessGeneral(Request $request){
        dd('coming soon');
    }

    //all mobile no download view
    public function downloadMobile(){
        $this->data['main_menu'] = 'MOBILE_DOWNLOAD';
        $this->data['sub_menu'] = 'MOBILE_DOWNLOAD';
        $unionPorishod=UnionPorishod::where('status',1)->orderBy('id','DESC')->get();
        $word=Word::where('status',1)->orderBy('id','DESC')->get();
        $this->data['main_menu'] = 'HOME_SETUP';
        $this->data['sub_menu'] = 'HOME_SETUP';
        return view('backend.sms.sms_send.download_mobile',$this->data,compact('unionPorishod','word'));
    }

    //download mobile union 
    public function downloadMobileUnion(Request $request){
       $id=$request->union_id;
       $data=UnionAdmin::where('up_id',$id)->select('mobile_no')->get();
       return Excel::download(new UnionMobileExport($data),'mobile_excel.xlsx');
    } 

     //download mobile word 
     protected  function downloadMobileWord(Request $request){
        $id=$request->word_id;
        $data=GeneralHolding::where('ward_id',$id)->select('mobile')->get()->toArray();
        // $pouroshova=PouroshovaAccessor::where('word_id',$id)->select('mobile_no')->get();
        return Excel::download(new WordMobileExport($data),'mobile_excel_word.xlsx');   
    } 


}
