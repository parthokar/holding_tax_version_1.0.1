<?php

namespace App\Http\Controllers\user_store;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use Illuminate\Http\Request;
use App\Traits\UserStoreTrait;
use App\Models\User;
use session;

class UserStoreController extends Controller
{
    use UserStoreTrait;

    public function formChange(Request $request){
        $this->data['main_menu'] = 'User';
        $this->data['sub_menu'] = 'User';
        if($request->select==''){
            return redirect()->route('create.user');
        }
       if($request->select==1){
           $type=1;
           return view('backend.user_form.1',compact('type'),$this->data);
       }
       if($request->select==2){
        $type=2;
        return view('backend.user_form.2',compact('type'), $this->data);
       }
       if($request->select==3){
        $type=3;   
        return view('backend.user_form.3',$this->data,compact('type'));
       }
       if($request->select==4){
        $type=4;   
        return view('backend.user_form.4',$this->data,compact('type'));
       }
       if($request->select==5){
        return view('backend.user_form.5',$this->data);
       }
       if($request->select==6){
        return view('backend.user_form.6',$this->data);
       }
       if($request->select==7){
        return view('backend.user_form.7',$this->data);
       }
       if($request->select==8){
        return view('backend.user_form.8',$this->data);
       }
    }

    public function userStore(UserFormRequest $request,$type) {
        $this->storeUser($request,$type);
    }

}
