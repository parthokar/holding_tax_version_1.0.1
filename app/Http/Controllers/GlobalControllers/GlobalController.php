<?php

namespace App\Http\Controllers\GlobalControllers;

use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\Upazila;
use Illuminate\Http\Request;

class GlobalController extends Controller
{
    public function getUpazilasByDistrictId($id)
    {
        return Upazila::where('district_id',$id)->get();
    }
    
}
