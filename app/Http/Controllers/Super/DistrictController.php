<?php

namespace App\Http\Controllers\Super;

use App\Models\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\DistrictRequest;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['main_menu'] = 'Settings';
        $this->data['sub_menu'] = 'District';
        return view()->exists('backend.district.index') ? view('backend.district.index',$this->data) : abort(404);
    }

   public function getAllDistrict(){
        return District::latest()->get();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DistrictRequest $request)
    {
        District::create([
            'name' => $request->input('name'),
            'created_by' => auth()->id()
        ]);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return District::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //info($request);
        $dis =   District::find($id);
        $dis->name = $request->input('name');
        $dis->status = $request->input('status');
        $dis->save();
    }

}
