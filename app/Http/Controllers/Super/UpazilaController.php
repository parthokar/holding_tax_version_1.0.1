<?php

namespace App\Http\Controllers\Super;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpazilaRequest;
use App\Models\District;
use App\Models\Upazila;
use Illuminate\Http\Request;

class UpazilaController extends Controller
{
    public function index()
    {
        $this->data['main_menu'] = 'Settings';
        $this->data['sub_menu'] = 'Upazila';
        $this->data['districts'] = District::latest()->get();
        return view()->exists('backend.upazila.index') ? view('backend.upazila.index',$this->data) : abort(404);
    }

    public function getAllUpazila()
    {
        return Upazila::with('district')->latest()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $upazilas = array_filter($request->upazila, function ($filter) {
            return !empty($filter);
        });
        for ($i=0; $i < count($upazilas) ; $i++) {
            $up = new Upazila();
            $up->district_id = $request->input('district_id');
            $up->name = $upazilas[$i];
            $up->created_by = auth()->id();
            $up->save();
        }
        $this->setSuccessMessageForDashboard('সফলভাবে তৈরি করা হয়েছে!');
        return back();
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Upazila::with('district')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $des =   Upazila::find($id);
        $des->name = $request->input('name');
        $des->district_id = $request->input('district_id');
        $des->status = $request->input('status');
        $des->save();
    }
}
