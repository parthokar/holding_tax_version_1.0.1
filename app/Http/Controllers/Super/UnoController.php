<?php

namespace App\Http\Controllers\Super;

use App\Models\Uno;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UnoRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UnoController extends Controller
{
    public function store(UnoRequest $request)
    {
        $request->validated();
        $user = new User();
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->user_type = 3;
        $user->status = 1;
        $user->password = Hash::make($request->input('password'));
        $user->save();
        Uno::create([
            'district_id' => $request->input('district_id'),
            'upazila_id' => $request->input('upazila_id'),
            'designation_id' => $request->input('designation_id'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'post_code' => $request->input('post_code'),
            'mobile' => $request->input('mobile'),
            'password' => Hash::make($request->input('password')),
            'user_id' => $user->id,
            'created_by' => auth()->guard('web')->id()
        ]);
        return response([
            'status' => 201,
            'message' => ' সফলভাবে তৈরি করা হয়েছে!'
        ]);
        //return redirect()->back();
    }
}
