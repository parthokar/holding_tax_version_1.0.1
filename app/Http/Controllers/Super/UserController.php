<?php

namespace App\Http\Controllers\Super;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function createUser(){
        $this->data['main_menu'] = 'User';
        $this->data['sub_menu'] = 'User';
       return view('backend.user.create', $this->data);
    }
}
