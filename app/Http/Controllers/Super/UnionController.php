<?php

namespace App\Http\Controllers\Super;

use App\Models\Union;
use App\Models\District;
use Illuminate\Http\Request;
use App\Http\Requests\UnionRequest;
use App\Http\Controllers\Controller;

class UnionController extends Controller
{
    public function index()
    {
        $this->data['main_menu'] = 'Settings';
        $this->data['sub_menu'] = 'Union';
        $this->data['districts'] = District::latest()->get();
        return view()->exists('backend.union.index') ? view('backend.union.index', $this->data) : abort(404);
    }

    public function getAllUnion()
    {
        return Union::with('district','upazila')->latest()->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unions = array_filter($request->union, function ($filter) {
            return !empty($filter);
        });
        for ($i = 0; $i < count($unions); $i++) {
            $up = new Union();
            $up->district_id = $request->input('district_id');
            $up->upazila_id = $request->input('upazila_id');
            $up->name = $unions[$i];
            $up->created_by = auth()->id();
            $up->save();
        }
        //$create = Upazila::create($request->validated());
        $this->setSuccessMessageForDashboard('সফলভাবে তৈরি করা হয়েছে!!');
        return back();
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Union::with('district', 'upazila')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $des = Union::find($id);
        $des->name = $request->input('name');
        $des->district_id = $request->input('district_id');
        $des->upazila_id = $request->input('upazila_id');
        $des->status = $request->input('status');
        $des->save();
    }
}
