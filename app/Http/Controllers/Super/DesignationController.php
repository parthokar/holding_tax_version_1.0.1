<?php

namespace App\Http\Controllers\Super;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddDesignatinRequest;
use App\Models\Designation;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['main_menu'] = 'Settings';
        $this->data['sub_menu'] = 'Designation';
        $list=Designation::latest()->get();
        return view('backend.designation.index',compact('list'), $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddDesignatinRequest $request)
    {
        $store=new Designation;
        $store->name=$request->name;
        $store->created_by=auth()->user()->id;
        $store->status=1;
        $store->save();
        return redirect()->back()->with('success', 'পদবি সফলভাবে তৈরি করা হয়েছে');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['main_menu'] = 'Settings';
        $this->data['sub_menu'] = 'Designation';
        $list=Designation::latest()->get();
        $edit=Designation::find($id);
        return view('backend.designation.edit',compact('list','edit'),$this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $update=Designation::find($id);
      $update->name = $request->name;
      $update->status = $request->status;
      $update->created_by = auth()->user()->id;
      $update->save();
      return redirect()->back()->with('success', 'পদবি সফলভাবে আপডেট করা হয়েছে');
    }
}
