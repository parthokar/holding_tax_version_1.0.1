<?php

namespace App\Http\Controllers\pouroshova;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pouroshova;
use session;

class PouroshovaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['main_menu'] = 'Settings';
        $this->data['sub_menu'] = 'Pouroshova';
        $list=Pouroshova::orderBy('id','DESC')->get();
        return view('backend.pouroshova.index',compact('list'),$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store= new Pouroshova;
        $store->pouroshova_name=$request->pouroshova_name;
        $store->status=1;
        $store->created_by=auth()->user()->id;
        $store->save();
        return redirect()->back()->with('success', 'পৌরসভা সফলভাবে তৈরি করা হয়েছে');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['main_menu'] = 'Settings';
        $this->data['sub_menu'] = 'Pouroshova';
        $list=Pouroshova::orderBy('id','DESC')->get();
        $edit=Pouroshova::find($id);
        return view('backend.pouroshova.edit',compact('list','edit'),$this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update=Pouroshova::find($id);
        $update->pouroshova_name=$request->pouroshova_name;
        $update->status=$request->status;
        $update->created_by=auth()->user()->id;
        $update->save();
        return redirect()->back()->with('success', 'পৌরসভা সফলভাবে আপডেট করা হয়েছে');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
