<?php

namespace App\Http\Controllers\pouroshova;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccessorController extends Controller
{
    public function createAccessorForm(){
        $this->data['main_menu'] = 'PA';
        $this->data['sub_menu'] = 'PA';
        return view('backend.user_form.5',$this->data);
    }
}
