<?php

namespace App\Http\Controllers\ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Upazila;
use App\Models\Union;

class AjaxController extends Controller
{
    public function upazilla($id){
        $data=Upazila::where('district_id',$id)->get();
        return response()->json($data);
    }

    public function union($zid,$uid){
      $data=Union::where('district_id',$zid)->where('upazila_id',$uid)->get();
      return response()->json($data);
    }
}
