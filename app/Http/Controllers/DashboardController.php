<?php

namespace App\Http\Controllers;

use App\Models\Designation;
use App\Models\District;
use App\Models\Pouroshova;
use App\Models\Union;
use App\Models\Upazila;
use App\Models\User;
use App\Models\Word;
use Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function dashboard(){
        $this->data['main_menu'] ='Dashboard';
        $this->data['sub_menu'] = 'Dashboard';
        $this->data['user'] = User::count();
        $this->data['district'] = District::count();
        $this->data['upazila'] = Upazila::count();
        $this->data['pouroshova'] = Pouroshova::count();
        $this->data['union'] = Union::count();
        $this->data['word'] = Word::count();
        $this->data['designation'] = Designation::count();
        //$this->data['user'] = User::count();
        return view('backend.dashboard',$this->data);
    }

}
