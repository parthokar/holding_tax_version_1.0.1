<?php

namespace App\Http\Controllers\pouroshova_accessor;

use App\Models\Job;
use App\Models\Room;
use App\Models\User;
use App\Models\Word;
use App\Models\House;
use App\Models\TaxSetup;
use Illuminate\Http\Request;
use App\Models\GeneralHolding;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\GeneralHoldingRequest;

class TaxPayerController extends Controller
{
    public function showTaxPayerCreateForm()
    {
        $this->data['main_menu'] = 'TAX_PAYER';
        $this->data['sub_menu'] = 'TAX_PAYER';
        return view('backend.taxPayer.create',$this->data);
    }


    public function formChange(Request $request)
    {
        $this->data['main_menu'] = 'User';
        $this->data['sub_menu'] = 'User';
        if ($request->select == '') {
            return redirect()->route('taxPayer.create');
        }
        if ($request->select == 1) {
            $type = 1;
            $this->data['wards'] = Word::all();
            $this->data['houses'] = House::all();
            $this->data['rooms'] = Room::all();
            $this->data['jobs'] = Job::all();
            return view('backend.taxPayer.general', compact('type'), $this->data);
        }
        if ($request->select == 2) {
            $type =2;
            return view('backend.taxPayer.business', compact('type'), $this->data);
        }
    }

    public function storeGeneralHoldingTax(GeneralHoldingRequest $request)
    {
        $user = new User();
        $user->first_name = $request->name;
        $user->last_name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->mobile);
        $user->user_type = 2;
        $user->status = 1;
        $user->save();
        $tax = new GeneralHolding();
        $tax->user_id = $user->id;
        $tax->tax_way = $request->tax_way;
        $tax->ward_id = $request->ward_id;
        $tax->holding_no = $request->holding_no;
        $tax->village = $request->village;
        $tax->post_name = $request->post_name;
        $tax->post_code = $request->post_code;
        $tax->religion = $request->religion;
        $tax->gender = $request->gender;
        $tax->is_married = $request->is_married ? $request->is_married : null;
        $tax->name = $request->name;
        $tax->fathers_name = $request->fathers_name;
        $tax->husband_name = $request->husband_name ? $request->husband_name : null;
        $tax->nid = $request->nid;
        $tax->home_no = $request->home_no;
        $tax->home_conditions = $request->home_conditions;
        $tax->room_no = $request->room_no;
        $tax->job = $request->job;
        $tax->mobile = $request->mobile;
        $tax->email = $request->email;
        $tax->vumir_poriman = $request->vumir_poriman;
        $tax->vumir_poriman_shotok = $request->vumir_poriman_shotok;
        $tax->abadi_poriman = $request->abadi_poriman;
        $tax->sanitation = $request->sanitation;
        $tax->sompoti = $request->sompoti;
        $tax->family_member = $request->family_member;
        $tax->male = $request->male;
        $tax->female = $request->female;
        $tax->has_tubewell = $request->has_tubewell;
        $tax->has_electricty = $request->has_electricty;
        $tax->tax_value = $request->tax_value;
        $tax->last_tax_year = $request->last_tax_year;
        $tax->tax_per_year = $request->tax_per_year;
        if($tax->save()){
            $this->setSuccessMessageForDashboard('সফলভাবে তৈরি করা হয়েছে');
            return redirect()->back();
        }
    }

    public function getGeneralHoldingTax(Request $request)
    {
        $tax = TaxSetup::select('tax_amount', 'tax_percent')->where('job_id',$request->job_id)
            ->where('house_id', $request->house_id)
            ->where('room_id', $request->room_id)
            ->first();
            return $tax;
    }
}
