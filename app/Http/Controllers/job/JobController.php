<?php

namespace App\Http\Controllers\job;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\JobValidationRequest;

use App\Models\Job;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $job=Job::orderBy('id','DESC')->with('user')->get();

        $this->data['main_menu'] = 'JOB_SETUP';

        $this->data['sub_menu'] = 'JOB_SETUP';

        return view('backend.job.index',$this->data,compact('job'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobValidationRequest $request)
    {
        $store = new Job;

        $store->job_type=$request->job_type; 

        $store->created_by=auth()->user()->id;

        $store->save(); 

        return redirect()->back()->with('success', 'সফলভাবে তৈরি করা হয়েছে');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job=Job::orderBy('id','DESC')->with('user')->get();

        $edit=Job::find($id);

        $this->data['main_menu'] = 'JOB_SETUP';
        $this->data['sub_menu'] = 'JOB_SETUP';
        return view('backend.job.edit',$this->data,compact('job','edit'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobValidationRequest $request, $id)
    {
        $edit=Job::find($id);

        $edit->job_type=$request->job_type;

        $edit->status=$request->status;

        $edit->save();

        return redirect()->back()->with('success', 'সফলভাবে আপডেট করা হয়েছে');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
