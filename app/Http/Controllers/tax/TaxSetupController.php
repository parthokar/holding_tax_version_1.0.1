<?php

namespace App\Http\Controllers\tax;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\TaxSetupValidationRequest;

use App\Models\House;

use App\Models\Room;

use App\Models\Job;

use App\Models\TaxSetup;

class TaxSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $house=House::where('status',1)->orderBy('id','DESC')->get();

        $room=Room::where('status',1)->orderBy('id','DESC')->get();

        $job=Job::where('status',1)->orderBy('id','DESC')->get();

        $tax=TaxSetup::orderBy('id','DESC')->with('house','room','job')->get();

        $this->data['main_menu'] = 'TAX_SETUP';
        $this->data['sub_menu'] = 'TAX_SETUP';
        return view('backend.taxSetup.index',$this->data,compact('house','room','job','tax'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaxSetupValidationRequest $request)
    {
        $store = new TaxSetup;

        $store->house_id=$request->house_id;

        $store->room_id=$request->room_id;

        $store->job_id=$request->job_id;

        $store->tax_amount=$request->tax_amount;

        $store->created_by=auth()->user()->id;

        $store->status=1;

        $store->save();

        return redirect()->back()->with('success', 'সফলভাবে তৈরি করা হয়েছে');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit=TaxSetup::find($id);

        $house=House::where('status',1)->orderBy('id','DESC')->get();

        $room=Room::where('status',1)->orderBy('id','DESC')->get();

        $job=Job::where('status',1)->orderBy('id','DESC')->get();

        $tax=TaxSetup::orderBy('id','DESC')->with('house','room','job')->get();

        $this->data['main_menu'] = 'TAX_SETUP';
        $this->data['sub_menu'] = 'TAX_SETUP';
        return view('backend.taxSetup.edit',$this->data,compact('house','room','job','tax','edit'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaxSetupValidationRequest $request, $id)
    {
        $edit=TaxSetup::find($id);

        $edit->house_id=$request->house_id;

        $edit->room_id=$request->room_id;

        $edit->job_id=$request->job_id;

        $edit->tax_amount=$request->tax_amount;

        $edit->created_by=auth()->user()->id;

        $edit->status=$request->status;

        $edit->save();

        return redirect()->back()->with('success', 'সফলভাবে আপডেট করা হয়েছে');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
