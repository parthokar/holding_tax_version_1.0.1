<?php

namespace App\Http\Controllers;

use App\Models\DcAdmin;
use App\Models\Pouroshova;
use App\Models\PouroshovaAccessor;
use App\Models\PouroshovaAdmin;
use App\Models\PouroshovaTaxCollector;
use App\Models\UnionAccessor;
use App\Models\UnionAdmin;
use App\Models\UnionTaxCollector;
use App\Models\UnoAdmin;
use App\Models\User;
use App\Traits\UploadImageTrait;
use Illuminate\Http\Request;

class UpdateProfileController extends Controller
{
    use UploadImageTrait;
    public function showUpdateProfileForm(){
        if(auth()->user()->user_type === 1){
            abort(404);
        }
         $this->data['data'] = getUpdateUserData();
        $this->data['auth'] = auth()->user();
        $this->data['main_menu'] = 'Profile_change';
        $this->data['sub_menu'] = 'Profile_change';
        return view('backend.profile.update',$this->data);
    }
    public function updateUserProfile(Request $request,$id,$type)
    {
       // info($request->office_logo);
       //return $request->office_logo->extension();
        if ($type == 2) {
            $user = DcAdmin::find($id);
            $oldImg = $user->image;
            $oldLogo = $user->office_logo;
            $user->mobile_no = $request->input('mobile_no');
            $user->save();
            $main_user =  User::find($user->user_id);
            $main_user->first_name = $request->input('first_name');
            $main_user->last_name = $request->input('last_name');
            $main_user->save();
            if ($request->image) {
                $img = $request->file('image');
                if($this->userImageUpload($img, 'uploads/dc/', 'image', $user)){
                    if(file_exists($oldImg)){
                        unlink($oldImg);
                    }
                }
            }
            if($request->office_logo) {
                $img = $request->file('office_logo');
                if ($this->userImageUpload($img, 'uploads/dc/', 'office_logo', $user)) {
                    if (file_exists($oldLogo)) {
                        unlink($oldLogo);
                    }
                }
            }
            $this->setErrorMessageForDashboard('সফলভাবে আপডেট করা হয়েছে');
            return back();
        }
        if($type == 3){
            $user = UnoAdmin::find($id);
            $oldImg = $user->image;
            $oldLogo = $user->office_logo;
            $user->mobile_no = $request->input('mobile_no');
            $user->save();
            $main_user =  User::find($user->user_id);
            $main_user->first_name = $request->input('first_name');
            $main_user->last_name = $request->input('last_name');
            $main_user->save();
            if ($request->image) {
                $img = $request->file('image');
                if ($this->userImageUpload($img, 'uploads/uno/', 'image', $user)) {
                    if (file_exists($oldImg)) {
                        unlink($oldImg);
                    }
                }
            }
            if ($request->office_logo) {
                $img = $request->file('office_logo');
                if ($this->userImageUpload($img, 'uploads/uno/', 'office_logo', $user)) {
                    if (file_exists($oldLogo)) {
                        unlink($oldLogo);
                    }
                }
            }
            $this->setErrorMessageForDashboard('সফলভাবে আপডেট করা হয়েছে');
            return back();
        }
        if ($type == 4) {
            $user = PouroshovaAdmin::find($id);
            $oldImg = $user->image;
            $oldLogo = $user->office_logo;
            $user->mobile_no = $request->input('mobile_no');
            $user->save();
            $main_user =  User::find($user->user_id);
            $main_user->first_name = $request->input('first_name');
            $main_user->last_name = $request->input('last_name');
            $main_user->save();
            if ($request->image) {
                $img = $request->file('image');
                if ($this->userImageUpload($img, 'uploads/pouroshova/', 'image', $user)) {
                    if (file_exists($oldImg)) {
                        unlink($oldImg);
                    }
                }
            }
            if ($request->office_logo) {
                $img = $request->file('office_logo');
                if ($this->userImageUpload($img, 'uploads/pouroshova/', 'office_logo', $user)) {
                    if (file_exists($oldLogo)) {
                        unlink($oldLogo);
                    }
                }
            }
            $this->setErrorMessageForDashboard('সফলভাবে আপডেট করা হয়েছে');
            return back();
        }
        if ($type == 5) {
            $user = UnionAdmin::find($id);
            $oldImg = $user->image;
            $oldLogo = $user->office_logo;
            $user->mobile_no = $request->input('mobile_no');
            $user->save();
            $main_user =  User::find($user->user_id);
            $main_user->first_name = $request->input('first_name');
            $main_user->last_name = $request->input('last_name');
            $main_user->save();
            if ($request->image) {
                $img = $request->file('image');
                if ($this->userImageUpload($img, 'uploads/union_admin/', 'image', $user)) {
                    if (file_exists($oldImg)) {
                        unlink($oldImg);
                    }
                }
            }
            if ($request->office_logo) {
                $img = $request->file('office_logo');
                if ($this->userImageUpload($img, 'uploads/union_admin/', 'office_logo', $user)) {
                    if (file_exists($oldLogo)) {
                        unlink($oldLogo);
                    }
                }
            }
            $this->setErrorMessageForDashboard('সফলভাবে আপডেট করা হয়েছে');
            return back();
        }
        if ($type == 6) {
            $user = PouroshovaAccessor::find($id);
            $oldImg = $user->image;
            $oldLogo = $user->office_logo;
            $user->mobile_no = $request->input('mobile_no');
            $user->save();
            $main_user =  User::find($user->user_id);
            $main_user->first_name = $request->input('first_name');
            $main_user->last_name = $request->input('last_name');
            $main_user->save();
            if ($request->image) {
                $img = $request->file('image');
                if ($this->userImageUpload($img, 'uploads/pourosova_accessor/', 'image', $user)) {
                    if (file_exists($oldImg)) {
                        unlink($oldImg);
                    }
                }
            }
            if ($request->office_logo) {
                $img = $request->file('office_logo');
                if ($this->userImageUpload($img, 'uploads/pourosova_accessor/', 'office_logo', $user)) {
                    if (file_exists($oldLogo)) {
                        unlink($oldLogo);
                    }
                }
            }
            $this->setErrorMessageForDashboard('সফলভাবে আপডেট করা হয়েছে');
            return back();
        }
        if ($type == 7) {
            $user = UnionAccessor::find($id);
            $oldImg = $user->image;
            $oldLogo = $user->office_logo;
            $user->mobile_no = $request->input('mobile_no');
            $user->save();
            $main_user =  User::find($user->user_id);
            $main_user->first_name = $request->input('first_name');
            $main_user->last_name = $request->input('last_name');
            $main_user->save();
            if ($request->image) {
                $img = $request->file('image');
                if ($this->userImageUpload($img, 'uploads/union_accessor/', 'image', $user)) {
                    if (file_exists($oldImg)) {
                        unlink($oldImg);
                    }
                }
            }
            if ($request->office_logo) {
                $img = $request->file('office_logo');
                if ($this->userImageUpload($img, 'uploads/union_accessor/', 'office_logo', $user)) {
                    if (file_exists($oldLogo)) {
                        unlink($oldLogo);
                    }
                }
            }
            $this->setErrorMessageForDashboard('সফলভাবে আপডেট করা হয়েছে');
            return back();
        }
        if ($type == 8) {
            $user = PouroshovaTaxCollector::find($id);
            $oldImg = $user->image;
            $oldLogo = $user->office_logo;
            $user->mobile_no = $request->input('mobile_no');
            $user->save();
            $main_user =  User::find($user->user_id);
            $main_user->first_name = $request->input('first_name');
            $main_user->last_name = $request->input('last_name');
            $main_user->save();
            if ($request->image) {
                $img = $request->file('image');
                if ($this->userImageUpload($img, 'uploads/pourosova_tax/', 'image', $user)) {
                    if (file_exists($oldImg)) {
                        unlink($oldImg);
                    }
                }
            }
            if ($request->office_logo) {
                $img = $request->file('office_logo');
                if ($this->userImageUpload($img, 'uploads/pourosova_tax/', 'office_logo', $user)) {
                    if (file_exists($oldLogo)) {
                        unlink($oldLogo);
                    }
                }
            }
            $this->setErrorMessageForDashboard('সফলভাবে আপডেট করা হয়েছে');
            return back();
        }
        if ($type == 9) {
            $user = UnionTaxCollector::find($id);
            $oldImg = $user->image;
            $oldLogo = $user->office_logo;
            $user->mobile_no = $request->input('mobile_no');
            $user->save();
            $main_user =  User::find($user->user_id);
            $main_user->first_name = $request->input('first_name');
            $main_user->last_name = $request->input('last_name');
            $main_user->save();
            if ($request->image) {
                $img = $request->file('image');
                if ($this->userImageUpload($img, 'uploads/union_tax/', 'image', $user)) {
                    if (file_exists($oldImg)) {
                        unlink($oldImg);
                    }
                }
            }
            if ($request->office_logo) {
                $img = $request->file('office_logo');
                if ($this->userImageUpload($img, 'uploads/union_tax/', 'office_logo', $user)) {
                    if (file_exists($oldLogo)) {
                        unlink($oldLogo);
                    }
                }
            }
            $this->setErrorMessageForDashboard('সফলভাবে আপডেট করা হয়েছে');
            return back();
        }
    }

}

