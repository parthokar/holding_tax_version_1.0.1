<?php

namespace App\Http\Controllers\house;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\HouseValidationRequest;

use App\Models\House;

class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $house=House::orderBy('id','DESC')->with('user')->get();

        $this->data['main_menu'] = 'HOME_SETUP';

        $this->data['sub_menu'] = 'HOME_SETUP';

        return view('backend.house.index',$this->data,compact('house'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HouseValidationRequest $request)
    {
        $store = new House;

        $store->house_name=$request->house_name; 

        $store->created_by=auth()->user()->id;

        $store->save(); 

        return redirect()->back()->with('success', 'সফলভাবে তৈরি করা হয়েছে');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $house=House::orderBy('id','DESC')->get();

        $edit=House::find($id);

        $this->data['main_menu'] = 'HOME_SETUP';
        $this->data['sub_menu'] = 'HOME_SETUP';
        return view('backend.house.edit',$this->data,compact('house','edit'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HouseValidationRequest $request, $id)
    {
        $edit=House::find($id);

        $edit->house_name=$request->house_name;

        $edit->status=$request->status;

        $edit->save();

        return redirect()->back()->with('success', 'সফলভাবে আপডেট করা হয়েছে');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
