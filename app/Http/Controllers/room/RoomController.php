<?php

namespace App\Http\Controllers\room;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\RoomValidationRequest;

use App\Models\Room;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $room=Room::orderBy('id','DESC')->with('user')->get();

        $this->data['main_menu'] = 'ROOM_SETUP';

        $this->data['sub_menu'] = 'ROOM_SETUP';

        return view('backend.room.index',$this->data,compact('room'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoomValidationRequest $request)
    {
        $store = new Room;

        $store->room_name=$request->room_name; 

        $store->created_by=auth()->user()->id;

        $store->save(); 

        return redirect()->back()->with('success', 'সফলভাবে তৈরি করা হয়েছে');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room=Room::orderBy('id','DESC')->with('user')->get();

        $edit=Room::find($id);

        $this->data['main_menu'] = 'ROOM_SETUP';
        $this->data['sub_menu'] = 'ROOM_SETUP';
        return view('backend.Room.edit',$this->data,compact('room','edit'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoomValidationRequest $request, $id)
    {
        $edit=Room::find($id);

        $edit->room_name=$request->room_name;

        $edit->status=$request->status;

        $edit->save();

        return redirect()->back()->with('success', 'সফলভাবে আপডেট করা হয়েছে');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
