<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use session;
use Redirect;
use App\User;

class UserAccessController extends Controller
{

    //super login view
     public function superView(){
        if(!Auth::user()){
            return view('backend.login.super');
        }else{
            return redirect()->route('user.dashboard');
        } 
     }
     
    //dc login view
    public function dcView(){
        if(!Auth::user()){
            return view('backend.login.dc');
        }else{
            return redirect()->route('user.dashboard');
        } 
     }
     
    //uno login view
    public function unoView(){
        if(!Auth::user()){
            return view('backend.login.uno');
        }else{
            return redirect()->route('user.dashboard');
        } 
     }
     
    //pouroshova login view
    public function pouroshovaView(){
        if(!Auth::user()){
            return view('backend.login.pouroshova');
        }else{
            return redirect()->route('user.dashboard');
        } 
     }
     
    //union login view
    public function unionView(){
        if(!Auth::user()){
            return view('backend.login.union');
        }else{
            return redirect()->route('user.dashboard');
        } 
     }
     
    //pouroshova accessor login view
    public function paccessorView(){
        if(!Auth::user()){
            return view('backend.login.pouroshova_accessor');
        }else{
            return redirect()->route('user.dashboard');
        } 
     }
     
    //union accessor  login view
    public function uaccessorView(){
        if(!Auth::user()){
            return view('backend.login.union_accessor');
        }else{
            return redirect()->route('user.dashboard');
        } 
     }
     
    //pouroshova tax collector login view
    public function ptaxcollectorView(){
        if(!Auth::user()){
            return view('backend.login.pouroshova_tax_collector');
        }else{
            return redirect()->route('user.dashboard');
        } 
     }
     
    //union tax collector login view
    public function utaxcollectorView(){
        if(!Auth::user()){
            return view('backend.login.union_tax_collector');
        }else{
            return redirect()->route('user.dashboard');
        } 
     }

      // user login
      public function login (Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('errors',$validator->messages());
        }
        $email=$request->email;
        $password=$request->password;
        if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1])) {
           return redirect()->route('user.dashboard');
        }else{
            return redirect()->back()->with('error', 'Wrong email & password');
        }
    }

    //user logout
    public function userLogout(){
        $type=auth()->user()->user_type;
        if($type==1){
            Auth::logout();
            return redirect()->to('super/admin');
            exit();
        }
        if($type==2){
            Auth::logout();
            return redirect()->to('dc/admin');
            exit();
        }
        if($type==3){
            Auth::logout();
            return redirect()->to('uno/admin');
            exit();
        }
        if($type==4){
            Auth::logout();
            return redirect()->to('/pouroshova/admin');
            exit();
        }
        if($type==5){
            Auth::logout();
            return redirect()->to('/union/admin');
            exit();
        }
        if($type==6){
            Auth::logout();
            return redirect()->to('/pouroshova/accessor/admin');
            exit();
        }
        if($type==7){
            Auth::logout();
            return redirect()->to('/union/accessor/admin');
            exit();
        }
        if($type==8){
            Auth::logout();
            return redirect()->to('pouroshova/tax/collector/admin');
            exit();
        }
        if($type==9){
            Auth::logout();
            return redirect()->to('/union/tax/collector/admin');
            exit();
        }
    }
}
