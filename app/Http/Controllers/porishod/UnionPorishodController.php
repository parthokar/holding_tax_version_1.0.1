<?php

namespace App\Http\Controllers\porishod;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UnionPorishod;
use session;

class UnionPorishodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['main_menu'] = 'Settings';
        $this->data['sub_menu'] = 'UP';
        $list=UnionPorishod::orderBy('id','DESC')->get();
        return view('backend.union_porishod.index',compact('list'),$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store= new UnionPorishod;
        $store->union_porishod_name=$request->union_porishod_name;
        $store->status=1;
        $store->created_by=auth()->user()->id;
        $store->save();
        return redirect()->back()->with('success', 'ইউনিয়ন পরিষদ সফলভাবে তৈরি করা হয়েছে');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['main_menu'] = 'Settings';
        $this->data['sub_menu'] = 'UP';
        $list=UnionPorishod::orderBy('id','DESC')->get();
        $edit=UnionPorishod::find($id);
        return view('backend.union_porishod.edit',compact('list','edit'),$this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update=UnionPorishod::find($id);
        $update->union_porishod_name=$request->union_porishod_name;
        $update->status=$request->status;
        $update->created_by=auth()->user()->id;
        $update->save();
        return redirect()->back()->with('success', 'ইউনিয়ন পরিষদ সফলভাবে আপডেট করা হয়েছে');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
