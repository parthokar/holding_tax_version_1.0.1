<?php

use App\Models\Dc;
use App\Models\PouroshovaAccessor;
use App\Models\PouroshovaAdmin;
use App\Models\UnionAccessor;
use App\Models\UnionAdmin;
use App\Models\UnionTaxCollector;
use App\Models\Uno;
use App\Models\User;

function test()
{
    return 'hello';
}

//this function for check a user super admin or not
if (!function_exists('isCredentialsSuperAdmin')) {
    function isCredentialsSuperAdmin(): bool
    {
        if(auth()->guard('web')->user()->user_type === 'super-admin'){
            return true;
        }
        return false;
    }
}
if (!function_exists('isCredentialsDcAdmin')) {
    function isCredentialsDcAdmin(): bool
    {
        if(auth()->user()->user_type === 'dc'){
            return true;
        }
        return false;
    }
}

