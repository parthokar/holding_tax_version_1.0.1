<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\Designation;
use App\Models\District;
use App\Models\Pouroshova;
use App\Models\UnionPorishod;
use App\Models\Word;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $designation=Designation::orderBy('id','DESC')->get();
        view()->share('designation',$designation);

        $zilla=District::orderBy('id','DESC')->get();
        view()->share('zilla',$zilla);

        $pouroshova=Pouroshova::orderBy('id','DESC')->get();
        view()->share('pouroshova',$pouroshova);

        $unionPorishod=UnionPorishod::orderBy('id','DESC')->get();
        view()->share('unionPorishod',$unionPorishod);

        $word=Word::orderBy('id','DESC')->get();
        view()->share('word',$word);
    }
}
