<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxSetup extends Model
{

  //this function show tax house name
   public function house(){
      return $this->belongsTo(House::class,'house_id');
   }

   //this function show tax room name
   public function room(){
      return $this->belongsTo(Room::class,'room_id');
   }

   //this function show tax job name
   public function job(){
      return $this->belongsTo(Job::class,'job_id');
   }

   //this function show tax created user name
   public function user(){
    return $this->belongsTo(User::class,'created_by');
   }
}
