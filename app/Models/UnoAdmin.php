<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnoAdmin extends Model
{
    use HasFactory;
    protected $casts = [
        'expired_date' => 'datetime:Y-m-d',
    ];
}
