<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Upazila extends Model
{
    use HasFactory;
    protected $fillable = ['name','district_id','status','created_by'];
    public function district(){
        return $this->belongsTo(District::class, 'district_id','id');
    }
}
