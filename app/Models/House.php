<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Users;

class House extends Model
{

   //this function show house created user name
   public function user(){
      return $this->belongsTo(User::class,'created_by');
   }

}
