<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Uno extends Model
{
    use HasFactory;
    protected $fillable = ['district_id', 'upazila_id', 'designation_id', 'user_id', 'first_name', 'last_name', 'email', 'mobile', 'logo', 'image', 'user_type', 'status','post_code','password'];
}

