<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //this function show room created user name
    public function user(){
        return $this->belongsTo(User::class,'created_by');
    }
    
}
