<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDcAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dc_admins', function (Blueprint $table) {
            $table->id();
            $table->unsignedbiginteger('user_id');
            $table->unsignedbiginteger('zilla_id');
            $table->unsignedbiginteger('designation_id');
            $table->string('mobile_no',100)->nullable();
            $table->string('nid',30)->default('1522222222222');
            $table->string('office_logo',100)->nullable();
            $table->string('image',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dc_admins');
    }
}
