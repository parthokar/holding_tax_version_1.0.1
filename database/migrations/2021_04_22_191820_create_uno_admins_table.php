<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnoAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uno_admins', function (Blueprint $table) {
            $table->id();
            $table->unsignedbiginteger('user_id');
            $table->unsignedbiginteger('zilla_id');
            $table->unsignedbiginteger('upazilla_id');
            $table->unsignedbiginteger('designation_id');
            $table->string('post_code',100)->nullable();
            $table->string('mobile_no',100)->nullable();
            $table->string('nid', 30)->default('1522222222222');
            $table->string('image',100)->nullable();
            $table->string('office_logo', 100)->nullable();
            /*
            $table->dateTime('expired_date');
            $table->enum('charge_type',['monthly','yearly']);
            $table->float('charge',15);
            */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uno_admins');
    }
}
