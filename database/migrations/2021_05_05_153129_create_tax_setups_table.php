<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_setups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('house_id')->index()->nullable();
            $table->unsignedBigInteger('room_id')->index()->nullable();
            $table->unsignedBigInteger('job_id')->index()->nullable();
            $table->integer('tax_percent')->default(0);
            $table->decimal('tax_amount',18,2)->default(0);
            $table->unsignedBigInteger('created_by')->index()->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_setups');
    }
}
