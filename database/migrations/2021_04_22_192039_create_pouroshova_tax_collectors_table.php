<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePouroshovaTaxCollectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pouroshova_tax_collectors', function (Blueprint $table) {
            $table->id();
            $table->unsignedbiginteger('user_id');
            $table->unsignedbiginteger('zilla_id');
            $table->unsignedbiginteger('pouroshova_id');
            $table->unsignedbiginteger('word_id');
            $table->unsignedbiginteger('designation_id');
            $table->string('post_code',100)->nullable();
            $table->string('mobile_no',100)->nullable();
            $table->string('office_logo',100)->nullable();
            $table->string('image',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pouroshova_tax_collectors');
    }
}
