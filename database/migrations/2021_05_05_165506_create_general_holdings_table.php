<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralHoldingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_holdings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ward_id');
            $table->string('tax_way',15);
            $table->string('holding_no', 15);
            $table->string('village', 25);
            $table->string('post_name', 35);
            $table->string('post_code', 35);
            $table->string('religion', 35);
            $table->string('gender', 35);
            $table->boolean('is_married')->nullable();
            $table->string('name', 75);
            $table->string('fathers_name', 75)->nullable();
            $table->string('husband_name', 75)->nullable();
            $table->string('nid', 75);
            $table->string('home_no', 75)->nullable();
            $table->string('home_conditions', 75)->nullable();
            $table->integer('room_no')->nullable();
            $table->string('job', 75);
            $table->string('mobile', 75);
            $table->string('email', 75);
            $table->string('vumir_poriman', 75)->nullable();
            $table->string('vumir_poriman_shotok', 75)->nullable();
            $table->string('abadi_poriman', 75)->nullable();
            $table->string('sanitation', 75)->nullable();
            $table->string('sompoti', 75)->nullable();
            $table->integer('family_member')->nullable();
            $table->integer('male')->nullable();
            $table->integer('female')->nullable();
            $table->boolean('has_tubewell')->nullable();
            $table->boolean('has_electricty')->nullable();
            $table->float('tax_value')->nullable();
            $table->integer('last_tax_year')->nullable();
            $table->float('tax_per_year')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_holdings');
    }
}
