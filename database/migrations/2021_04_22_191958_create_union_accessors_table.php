<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnionAccessorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('union_accessors', function (Blueprint $table) {
            $table->id();
            $table->unsignedbiginteger('user_id');
            $table->unsignedbiginteger('zilla_id');
            $table->unsignedbiginteger('upazilla_id');
            $table->unsignedbiginteger('up_id');
            $table->unsignedbiginteger('word_id');
            $table->unsignedbiginteger('designation_id');
            $table->string('post_code',100)->nullable();
            $table->string('mobile_no',100)->nullable();
            $table->string('office_logo',100)->nullable();
            $table->string('image',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('union_accessors');
    }
}
