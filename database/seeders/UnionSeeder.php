<?php

namespace Database\Seeders;

use App\Models\Union;
use Illuminate\Database\Seeder;

class UnionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i <20 ; $i++) {
            $u =new Union();
            $u->name = 'ইউনিয়ন ' .$i;
            $u->district_id = rand(1,4);
            $u->upazila_id = rand(1, 13);
            $u->created_by = 1;
            $u->save();
        }
    }
}
