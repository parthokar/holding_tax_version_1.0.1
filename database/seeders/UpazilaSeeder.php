<?php

namespace Database\Seeders;

use App\Models\Upazila;
use Illuminate\Database\Seeder;

class UpazilaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $upazilas = ['দেবিদ্বার', 'বরুড়া', 'ব্রাহ্মণপাড়া', 'চান্দিনা', 'মতলব', 'হাজীগঞ্জ', 'মতলব', 'ফরিদগঞ্জ', 'সাভার', 'ধামরাই', 'কেরাণীগঞ্জ', 'নবাবগঞ্জ', 'দোহার'];
        for ($i = 0; $i < count($upazilas); $i++) {
            $a = new Upazila();
            $a->name = $upazilas[$i];
            $a->district_id = rand(1,4);
            $a->created_by = 1;
            $a->save();
        }
    }
}
