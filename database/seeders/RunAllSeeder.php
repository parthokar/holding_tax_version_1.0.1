<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RunAllSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DistrictSeeder::class,
            UpazilaSeeder::class,
            UnionSeeder::class,
            DesignationSeeder::class,
        ]);
    }
}
