<?php

namespace Database\Seeders;

use App\Models\Designation;
use Illuminate\Database\Seeder;

class DesignationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $d = ['ডিসি প্রশাসক', 'ইউনো প্রশাসক', 'পৌরসভা প্রশাসক', 'ইউনিয়ন প্রশাসক', 'পৌরসভা মূল্যায়নকারী', 'পৌরসভা কর সংগ্রাহক ', 'ইউনিয়ন কর সংগ্রাহক'];
        for ($i = 0; $i < count($d); $i++) {
            $a = new Designation();
            $a->name = $d[$i];
            $a->created_by =1;
            $a->save();
        }
    }
}
