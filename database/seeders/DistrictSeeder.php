<?php

namespace Database\Seeders;

use App\Models\District;
use Illuminate\Database\Seeder;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $districts = ['‎ঢাকা', 'কুমিল্লা', 'চাঁদপুর', 'সিলেট'];
        for ($i=0; $i < count($districts); $i++) {
            $a = new District();
            $a->name = $districts[$i];
            $a->created_by = 1;
            $a->save();
        }
    }
}
