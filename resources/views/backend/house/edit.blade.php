@extends('backend.layouts.master')
@section('title','বাড়ি')
@push('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush
@section('master_content')
<div class="container-fluld">
    <div class="row">
        <div class="col-12 col-md-8">
            <div class="card">
                @include('backend.includes.messages')
                <div class="card-header">
                    <h3 class="text-info">বাড়ি</h3>
                </div>
                <div class="card-body">
                     <div class="table-responsive">
                        <table id="myTable" class="table table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>বাড়ি</th>
                                    <th>তৈরি করেছেন</th>
                                    <th>অবস্থা</th>
                                    <th>আরও</th>
                                </tr>
                            </thead>
                             <tbody>
                                @foreach($house as $key=>$item)    
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$item->house_name}}</td>
                                    <td>{{$item->user->first_name}} {{$item->user->last_name}}</td>
                                    <td>@if($item->status==1) সক্রিয় @else নিষ্ক্রিয় @endif</td>
                                    <td>
                                        <a href="{{route('house.edit',$item->id)}}" class="btn btn-sm btn-info text-light">Edit</a>
                                    </td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4" id="addPortion">
            <div class="card">
                <div class="card-header">
                    <h5 class="text-info">নতুন বাড়ি</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('house.update',$edit->id) }}" id="addUnionFor" method="post">
                        @csrf
                        {{ method_field('PATCH') }}
                         <div class="form-group">
                            <label>বাড়ি</label> 
                            <input type="text" class="form-control" placeholder="বাড়ি নাম" id="name" name="house_name" value="{{$edit->house_name}}">
                         </div>
                         @if($errors->has('house_name'))
                         <div class="error">
                             {{$errors->first('house_name')}}
                         </div>
                         @endif
                         <div class="form-group">
                            <label>অবস্থা</label> 
                           <select class="form-control" name="status">
                               @if($edit->status==1)
                                  <option value="1" selected>সক্রিয়</option>
                                  <option value="0">নিষ্ক্রিয়</option>
                                @else 
                                  <option value="1">সক্রিয়</option>
                                  <option value="0" selected>নিষ্ক্রিয়</option>
                               @endif 
                           </select>
                         </div>
                         <div class="form-group">
                             <button class="btn btn-block btn-success" id="addButton">আপডেট</button>
                         </div>
                     </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script src="{{ asset('backend') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script>
      $('#myTable').DataTable();
</script>
@endpush

