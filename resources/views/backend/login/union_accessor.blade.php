@extends('backend.layouts.app')
@section('title','লগ-ইন')
@section('app_content')
<div class="login-page">
<div class="login-box">
  <div class="login-logo">
    {{-- <a href="../../index2.html"><b>Admin</b>LTE</a> --}}
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
         <div class="text-center mb-2">
             <img src="{{ asset('uploads/logo.png') }}" alt="" style="width: 60px" class="m-auto mb-2">
        </div>
      <p class="login-box-msg">লগ-ইন করুন</p>
      @include('backend.includes.messages')
      <form action="{{route('user.login')}}" method="post">
        @csrf
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="ইমেল">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="পাসওয়ার্ড">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                পাসওয়ার্ড মনে রাখুন
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">লগ-ইন</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
{{--
      <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div> --}}
      <!-- /.social-auth-links -->

      <p class="mb-1">
        <a href="#">পাসওয়ার্ড ভুলে গেছেন?</a>
      </p>
      {{-- <p class="mb-0">
        <a href="register.html" class="text-center">Register a new membership</a>
      </p> --}}
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
</div>

@stop
