@extends('backend.layouts.master')
@section('title','জেলা')
@push('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush
@section('master_content')
<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card">
                <div class="card-header d-flex" style="justify-content: space-between">
                    <h3 class="text-primary"> জেলা পরিচালনা করুন</h3>
                    <button class="btn btn-primary text-right"  data-toggle="modal" data-target="#addModal" style="margin-left:550px">নতুন জেলা</button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>জেলা</th>
                                     <th>অবস্থা</th>
                                    <th>আরও</th>
                                </tr>
                            </thead>
                            <tbody id="districtTable"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



      <div class="modal fade" id="addModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">নতুন জেলা</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                  <form action="" id="addDistrictForm">
                        <div class="form-group">
                            <label for=""> নাম : </label>
                            <input type="text" class="form-control" placeholder="জেলা নাম" id="name">
                            <span id="nameError" class="text-danger"></span>
                        </div>

                         <div class="form-group">
                            <button class="btn btn-block btn-success" id="addButton"> নতুন জেলা</button>
                    </div>
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
  <div class="modal fade" id="editModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="text-info">আপডেট জেলা</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                 <form action="" id="updateDistrictForm">
                        <div class="form-group">
                            <label for=""> নাম : </label>
                            <input type="text" class="form-control" placeholder="Enter Country Name" id="e_name">
                            <span id="nameError" class="text-danger"></span>
                        </div>
                        <div class="form-group">
                            <label for="">অবস্থা :</label>
                            <select name="" id="e_status" class="form-control">
                                <option value="1">সক্রিয়</option>
                                <option value="0">নিষ্ক্রিয়</option>
                            </select>
                        </div>
                         <input type="hidden" id="e_id">
                         <div class="form-group">
                            <button class="btn btn-block btn-success" id="addButton">আপডেট জেলা</button>
                        </div>
                    </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

@stop

@push('script')
<script src="{{ asset('backend') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script>
        function table_data_row(data) {
            var	rows = '';
            var i = 0;
            $.each( data, function( key, value ) {
                value.id
                rows += '<tr>';
                rows += '<td>'+ ++i +'</td>';
                rows += '<td>'+value.name+'</td>';
                 rows += '<td>'
                if(value.status == 0) {
                    rows += '<span class="badge badge-warning">নিষ্ক্রিয়</span>'
                }else if(value.status == 1){
                    rows += '<span class="badge badge-success">সক্রিয়</span>'
                }
                rows += '</td>'
                rows += '<td data-id="'+value.id+'" class="text-center">';
                rows += '<a class="btn btn-sm btn-info text-light" id="editRow" data-id="'+value.id+'" data-toggle="modal" data-target="#editModal">আপডেট</a> ';
                rows += '</td>';
                rows += '</tr>';
            });
            $("#districtTable").html(rows);
    }
        function getAllDistrict(){
            axios.get("{{ route('get-all-district') }}")
            .then(function(res){
               //console.log(res);
                table_data_row(res.data);
              $('.dataTable').DataTable();
            })
        }
        getAllDistrict();

        //store
         $('body').on('submit','#addDistrictForm',function(e){
        e.preventDefault();
        let nameError = $('#nameError')

        axios.post("{{ route('district.store') }}", {
            name: $('#name').val()
        })
        .then(function (response) {
            getAllDistrict();
             $('#name').val('')
          // console.log(response.data)
              setSwalMessage()
          $('#addModal').modal('toggle')
        })
        .catch(function (error) {
            if(error.response.data.errors.name){
                nameError.text(error.response.data.errors.name[0]);
            }
            console.log(error);
        });
    });



 //edit
    $('body').on('click','#editRow',function(){
        let id = $(this).data('id')
        let url = base_path + '/district' + '/'  + id + '/edit'
       // console.log(url);
        axios.get(url)
            .then(function(res){
                $('#e_name').val(res.data.name)
                $('#e_id').val(res.data.id)
                $('#e_status').val(res.data.status)
            })
    })

      //update
    $('body').on('submit','#updateDistrictForm',function(e){
        e.preventDefault()
        let id = $('#e_id').val()
        let data = {
            id : id,
            name : $('#e_name').val(),
            status : $('#e_status').val(),
        }
        let url = base_path + '/district' + '/'  + id
        axios.put(url,data)
        .then(function(res){
            getAllDistrict();
             $('#editModal').modal('toggle')
            setSwalMessage('success','Success','সফলভাবে তৈরি করা হয়েছে!');
            //console.log(res);
        })
    })
    </script>
@endpush

