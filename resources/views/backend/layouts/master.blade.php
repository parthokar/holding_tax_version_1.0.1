@extends('backend.layouts.app')
@section('app_content')

  <!-- Navbar -->
    @includeIf('backend.includes.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @includeIf('backend.includes.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content p-3">
  <div class="container">
         @if(session('dash_message'))
           <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
              {{ session('dash_message') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
        </div>
    @endif
    </div>
      <!-- Default box -->
        @yield('master_content')
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@includeIf('backend.includes.footer')
  <!-- /.control-sidebar -->
@stop
