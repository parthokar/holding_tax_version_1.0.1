@extends('backend.layouts.master')
@section('title','ওয়ার্ড')
@push('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush
@section('master_content')
<div class="container-fluld">
    <div class="row">
        <div class="col-12 col-md-8">
            <div class="card">
                @include('backend.includes.messages')
                <div class="card-header">
                    <h3 class="text-info">ওয়ার্ড</h3>
                </div>
                <div class="card-body">
                     <div class="table-responsive">
                        <table id="myTable" class="table table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>ওয়ার্ড</th>
                                    <th>অবস্থা</th>
                                    <th>আরও</th>
                                </tr>
                            </thead>
                             <tbody>
                             @foreach($list as $key=>$item)
                              <tr>
                                  <td>{{++$key}}</td>
                                  <td>{{$item->word_name}}</td>
                                  <td>@if($item->status==1) সক্রিয় @else নিষ্ক্রিয় @endif</td>
                                  <td>
                                      <a href="{{route('words.edit',$item->id)}}" class="btn btn-sm btn-info text-light">আপডেট</a>
                                  </td>
                              </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4" id="addPortion">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-info">নতুন ওয়ার্ড</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('words.store') }}" id="addUnionFor" method="post">
                        @csrf
                        <div class="input-group mb-3 dynamic_file">
                         <input type="text" class="form-control" placeholder="ওয়ার্ড নাম" id="name" name="word_name" required>
                        </div>
                         <div class="form-group">
                    <button class="btn btn-block btn-success" id="addButton">নতুন ওয়ার্ড</button>
                 </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script src="{{ asset('backend') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script>
      $('#myTable').DataTable();
</script>
@endpush

