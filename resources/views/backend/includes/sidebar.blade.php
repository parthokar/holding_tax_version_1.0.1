<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('backend') }}/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{auth()->user()->first_name}} {{auth()->user()->last_name}}</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          {{-- dashboard start --}}
          <li class="nav-item">
            <a href="{{url('/dashboard')}}" class="nav-link @if ($main_menu === 'Dashboard')
                active
            @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                ড্যাশবোর্ড
              </p>
            </a>
          </li>
          {{-- dashboard end --}}


         {{-- start include super admin permission --}}
            @include('backend.includes.permission.super')
         {{-- end include super admin permission --}}

         {{-- start include dc admin permission --}}
            @include('backend.includes.permission.dc')
         {{-- end include dc admin permission --}}

         {{-- start include dc admin permission --}}
            @include('backend.includes.permission.uno')
         {{-- end include dc admin permission --}}

         {{-- start include union admin permission --}}
            @include('backend.includes.permission.union')
         {{-- end include union admin permission --}}

        {{-- start include pouroshova admin permission --}}
            @include('backend.includes.permission.pourosova')
         {{-- end include pouroshova admin permission --}}

          {{-- start include pouroshova_accessor admin permission --}}
            @include('backend.includes.permission.pourosova_accessor')
         {{-- end include pouroshova admin permission --}}

         @if(!checkPermission(['super']))
           <li class="nav-item">
            <a href="{{ route('user.profile') }}" class="nav-link @if ($sub_menu === 'Profile_change')
                active
            @endif">
              <i class="nav-icon fas fa-user"></i>
              <p>প্রোফাইল পরিবর্তন করুন</p>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a href="{{ route('user.logout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>সাইন আউট</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
