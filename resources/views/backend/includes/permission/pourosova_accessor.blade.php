@if(checkPermission(['pouroshova-accessor']))
{{-- union porishod profile start --}}
   <li class="nav-item @if ($sub_menu === 'TAX_PAYER')
                active
            @endif">
      <a href="{{ route('taxPayer.create') }}" class="nav-link @if ($sub_menu === 'TAX_PAYER')
                active
            @endif">
     <i class="nav-icon fa fa-user" aria-hidden="true"></i>
     <p>
        করদাতা তৈরি
      <i class="fas fa-angle-left right"></i>
     </p>
   </a>
</li>
{{-- union porishod profile end --}}



@endif
