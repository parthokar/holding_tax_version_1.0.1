@if(checkPermission(['pouroshova']))
{{-- union porishod profile start --}}
   <li class="nav-item @if ($sub_menu === 'PA')
                active
            @endif">
      <a href="{{ route('accessor.create') }}" class="nav-link @if ($sub_menu === 'PA')
                active
            @endif">
     <i class="nav-icon fa fa-user" aria-hidden="true"></i>
     <p>
        অ্যাক্সেসর তৈরি
      <i class="fas fa-angle-left right"></i>
     </p>
   </a>
</li>
{{-- union porishod profile end --}}



@endif
