@if(checkPermission(['union']))
{{-- union porishod profile start --}}
   <li class="nav-item">
      <a href="#" class="nav-link">
     <i class="nav-icon fa fa-file" aria-hidden="true"></i>
     <p>
        ইউনিয়ন পরিষদ প্রোফাইল সেটআপ
      <i class="fas fa-angle-left right"></i>
     </p>
   </a>
</li>
{{-- union porishod profile end --}}

 {{-- accessor start --}}
 <li class="nav-item">
   <a href="#" class="nav-link">
  <i class="nav-icon fab fa-cc-amazon-pay"></i>
  <p>
    মূল্যায়নকারী
   <i class="fas fa-angle-left right"></i>
  </p>
</a>
 <ul class="nav nav-treeview">
   <li class="nav-item">
      <a href="#" class="nav-link">
        <i class="nav-icon far fa-user text-info"></i>
         <p>তালিকা</p>
      </a>
   </li>
   <li class="nav-item ">
    <a href="#" class="nav-link">
      <i class="nav-icon far fa-user text-info"></i>
        <p>তৈরি করুন</p>
    </a>
 </li>
</ul>
</li>
{{-- accessor end --}}

 {{-- collector start --}}
 <li class="nav-item">
    <a href="#" class="nav-link">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
    কালেক্টর
    <i class="fas fa-angle-left right"></i>
   </p>
 </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
       <a href="#" class="nav-link">
         <i class="nav-icon far fa-user text-info"></i>
          <p>তালিকা</p>
       </a>
    </li>
    <li class="nav-item ">
     <a href="#" class="nav-link">
       <i class="nav-icon far fa-user text-info"></i>
         <p>তৈরি করুন</p>
     </a>
  </li>
 </ul>
 </li>
 {{-- collector end --}}

  {{-- ward+village start --}}
  <li class="nav-item">
    <a href="#" class="nav-link">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
    ওয়ার্ড + গ্রাম সেটআপ
   </p>
 </a>
 </li>
 {{-- ward+village end --}}

  {{-- house start --}}
  <li class="nav-item">
    <a href="{{route('house.index')}}" class="nav-link @if($main_menu === 'HOME_SETUP')
    active
    @endif">
      <i class="nav-icon fas fa-cog"></i>
   <p>
    বাড়ি তৈরি করুন
   </p>
 </a>
 </li>
 {{-- house end --}}

 {{--room start --}}
   <li class="nav-item">
    <a href="{{route('room.index')}}" class="nav-link @if($main_menu === 'ROOM_SETUP')
    active
    @endif">
      <i class="nav-icon fas fa-cog"></i>
   <p>
    কক্ষ তৈরি করুন
   </p>
 </a>
 </li>
 {{-- room end --}}

  {{-- job/service start --}}
  <li class="nav-item">
    <a href="{{route('job.index')}}" class="nav-link @if($main_menu === 'JOB_SETUP')
    active
    @endif">
      <i class="nav-icon fas fa-cog"></i>
   <p>
    চাকরি / পেশা
   </p>
 </a>
 </li>
 {{-- job/service end --}}

 {{-- tax fees start --}}
   <li class="nav-item">
    <a href="{{route('taxsetup.index')}}" class="nav-link @if($main_menu === 'TAX_SETUP')
    active
    @endif">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
    ট্যাক্স ফি সেটআপ
   </p>
 </a>
 </li>
 {{-- tax fees end --}}

  {{-- collection report start --}}
  <li class="nav-item">
    <a href="#" class="nav-link">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
    রিপোর্ট সংগ্রহ
    <i class="fas fa-angle-left right"></i>
   </p>
 </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
       <a href="#" class="nav-link">
         <i class="nav-icon far fa-user text-info"></i>
          <p>জেনারেল হোল্ডিং বাই ওয়ার্ড / ডি 2 ডি / আজকে</p>
       </a>
    </li>
    <li class="nav-item ">
     <a href="#" class="nav-link">
       <i class="nav-icon far fa-user text-info"></i>
         <p>বিজনেস হোল্ডিং বাই ওয়ার্ড / ডি 2 ডি / আজকে</p>
     </a>
  </li>
 </ul>
 </li>
 {{-- collection report end --}}

{{-- due report start --}}
   <li class="nav-item">
    <a href="#" class="nav-link">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
    বকেয়া রিপোর্ট
    <i class="fas fa-angle-left right"></i>
   </p>
 </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
       <a href="#" class="nav-link">
         <i class="nav-icon far fa-user text-info"></i>
          <p>ট্যাক্স তালিকা ওয়ার্ড দ্বারা সাধারণ হোল্ডিং</p>
       </a>
    </li>
    <li class="nav-item ">
     <a href="#" class="nav-link">
       <i class="nav-icon far fa-user text-info"></i>
         <p>ওয়ার্ড অনুসারে করের তালিকার ব্যবসায় হোল্ডিং</p>
     </a>
  </li>
 </ul>
 </li>
 {{-- due report end --}}


 {{-- due report start --}}
 <li class="nav-item">
    <a href="#" class="nav-link">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
    এসএমএস পরিষেবা
    <i class="fas fa-angle-left right"></i>
   </p>
 </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
       <a href="#" class="nav-link">
         <i class="nav-icon far fa-user text-info"></i>
          <p>ওয়ার্ড সাধারণ হোল্ডিং</p>
       </a>
    </li>
    <li class="nav-item ">
     <a href="#" class="nav-link">
       <i class="nav-icon far fa-user text-info"></i>
         <p>ওয়ার্ড ব্যবসায় হোল্ডিং</p>
     </a>
  </li>
  <li class="nav-item ">
    <a href="#" class="nav-link">
      <i class="nav-icon far fa-user text-info"></i>
        <p>মোট কর প্রদানকারী / ওয়ার্ড (কাস্টম এসএমএস)</p>
    </a>
 </li>
 <li class="nav-item ">
    <a href="#" class="nav-link">
      <i class="nav-icon far fa-user text-info"></i>
        <p>সমস্ত বকেয়া করদাতার এসএমএস</p>
    </a>
 </li>
 </ul>
 </li>
 {{-- due report end --}}


{{-- edit assessment start --}}
<li class="nav-item">
    <a href="#" class="nav-link">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
    মূল্যায়ন সম্পাদনা করুন
    <i class="fas fa-angle-left right"></i>
   </p>
 </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
       <a href="#" class="nav-link">
         <i class="nav-icon far fa-user text-info"></i>
          <p>সাধারণ হোল্ডিং</p>
       </a>
    </li>
    <li class="nav-item ">
     <a href="#" class="nav-link">
       <i class="nav-icon far fa-user text-info"></i>
         <p>ব্যবসায় হোল্ডিং</p>
     </a>
  </li>
 </ul>
 </li>
 {{-- edit assessment end --}}

 {{-- information update start --}}
<li class="nav-item">
    <a href="#" class="nav-link">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
    তথ্য আপডেট অনুরোধ তালিকা
    <i class="fas fa-angle-left right"></i>
   </p>
 </a>
 </li>
 {{-- information update end --}}

  {{-- assessment list start --}}
<li class="nav-item">
    <a href="#" class="nav-link">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
     মূল্যায়ন তালিকা
    <i class="fas fa-angle-left right"></i>
   </p>
 </a>
 <ul class="nav nav-treeview">
    <li class="nav-item">
       <a href="#" class="nav-link">
         <i class="nav-icon far fa-user text-info"></i>
          <p>সাধারণ হোল্ডিং মূল্যায়ন তালিকা ওয়ার্ড নং দ্বারা</p>
       </a>
    </li>
    <li class="nav-item ">
     <a href="#" class="nav-link">
       <i class="nav-icon far fa-user text-info"></i>
         <p>ব্যবসায় হোল্ডিং মূল্যায়ন তালিকা ওয়ার্ড নং দ্বারা</p>
     </a>
  </li>
 </ul>
 </li>
 {{-- assessment list end --}}

  {{-- tax start --}}
  <li class="nav-item">
    <a href="#" class="nav-link">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
    কর সংগ্রহ
    <i class="fas fa-angle-left right"></i>
   </p>
 </a>
 <ul class="nav nav-treeview">
    <li class="nav-item">
       <a href="#" class="nav-link">
         <i class="nav-icon far fa-user text-info"></i>
          <p>সাধারণ কর</p>
       </a>
    </li>
    <li class="nav-item ">
     <a href="#" class="nav-link">
       <i class="nav-icon far fa-user text-info"></i>
         <p>ব্যবসায় কর</p>
     </a>
  </li>
 </ul>
 </li>
 {{-- tax end --}}

 {{-- search info start --}}
  <li class="nav-item">
    <a href="#" class="nav-link">
   <i class="nav-icon fab fa-cc-amazon-pay"></i>
   <p>
     তথ্য অনুসন্ধান
    <i class="fas fa-angle-left right"></i>
   </p>
 </a>
 </li>
 {{-- tax end --}}


@endif
