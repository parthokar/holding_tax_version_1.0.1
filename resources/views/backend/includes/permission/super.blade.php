    {{-- start super admin permission --}}
    @if(checkPermission(['super']))
    {{-- user start --}}
    <li class="nav-item @if ($main_menu === 'User')
          menu-open
      @endif">
      <a href="#" class="nav-link @if ($main_menu === 'User')
          active
      @endif">
        <i class="nav-icon fas fa-users"></i>
        <p>
          ব্যবহারকারী
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item ">
          <a href="{{ route('create.user') }}" class="nav-link @if ($main_menu === 'User')
          active
        @endif">
            <i class="nav-icon far fa-user text-info"></i>
            <p>নতুন ব্যবহারকারী</p>
          </a>
        </li>
      </ul>
    </li>
    {{-- user end --}}


    {{-- cart request strt --}}
      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fa fa-id-card" aria-hidden="true"></i>
          <p>
            ই-শেবা কার্ড
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>ইউনিয়ন > ওয়ার্ড দ্বারা</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>কার্ড ডাউনলোড</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>ডিএ্যাকটিভ ব্যবহারকারীর তালিকা</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>কার্ড পুনরায় মুদ্রণের অনুরোধ</p>
            </a>
          </li>
        </ul>
      </li>
    {{-- card request end --}}

    {{-- sms strt --}}
      <li class="nav-item @if ($main_menu === 'SMS_SETUP')
      menu-open
      @endif">
        <a href="#" class="nav-link @if ($main_menu === 'SMS_SETUP')
        active
    @endif">
          <i class="nav-icon fas fa-sms"></i>
          <p>
            খুদেবার্তা প্রেরণ
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{route('all.sms')}}" class="nav-link  @if ($main_menu === 'SMS_SETUP')
            active
          @endif">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>সব খুদেবার্তা প্রেরণ</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('download.mobile')}}" class="nav-link @if ($main_menu === 'MOBILE_DOWNLOAD')
          active
            @endif">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>মোবাইল নম্বর ডাউনলোড করুন</p>
            </a>
          </li>
        </ul>
      </li>
    {{-- sms end --}}

    {{-- deactive strt --}}
            <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-ban"></i>
          <p>
            ডিএ্যাকটিভ
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>ইউনিয়ন পরিষদ তালিকা</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>পৌরসভা তালিকা</p>
            </a>
          </li>
        </ul>
      </li>
    {{-- deactive end --}}


   <!-- settings start -->
    <li class="nav-item  @if ($main_menu === 'Settings')
          menu-open
      @endif">
      <a href="#" class="nav-link  @if ($main_menu === 'Settings')
          active
      @endif">
        <i class="nav-icon fas fa-cog"></i>
        <p>
          সেটিংস
          <i class="fas fa-angle-left right"></i>
        </p>
      </a>
      <ul class="nav nav-treeview">
        <li class="nav-item">
          <a href="{{ route('designation.index') }}" class="nav-link  @if ($sub_menu === 'Designation')
          active
      @endif">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>পদবি</p>
           </a>
         </li>
         <li class="nav-item">
             <a href="{{ route('district.index') }}" class="nav-link @if($sub_menu === 'District')
          active
      @endif" >
                 <i class="nav-icon far fa-circle text-info"></i>
                 <p>জেলা</p>
             </a>
         </li>
        <li class="nav-item">
          <a href="{{ route('upazila.index') }}" class="nav-link @if($sub_menu === 'Upazila')
          active
      @endif">
            <i class="nav-icon far fa-circle text-info"></i>
            <p>উপজেলা</p>
          </a>
        </li>
          <li class="nav-item">
          <a href="{{ route('union.index') }}" class="nav-link  @if($sub_menu === 'Union')
          active
      @endif">
            <i class="nav-icon far fa-circle text-info"></i>
            <p>ইউনিয়ন</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('pouroshova.index') }}" class="nav-link @if ($sub_menu === 'Pouroshova')
          active
      @endif">
            <i class="nav-icon far fa-circle text-info"></i>
            <p>পৌরসভা</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('union_porishod.index') }}" class="nav-link @if ($sub_menu === 'UP')
          active
      @endif">
            <i class="nav-icon far fa-circle text-info"></i>
            <p>ইউনিয়ন পরিষদ</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('words.index') }}" class="nav-link @if ($sub_menu === 'word')
          active
      @endif">
            <i class="nav-icon far fa-circle text-info"></i>
            <p>ওয়ার্ড</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('sms.index') }}" class="nav-link @if ($sub_menu === 'SMS_SETUP')
          active
      @endif">
            <i class="nav-icon far fa-circle text-info"></i>
            <p>এসএমএস চালু / বন্ধ</p>
          </a>
        </li>
      </ul>
    </li>
  {{-- settings end --}}
  @endif
 {{-- end super admin permision --}}