    @if(checkPermission(['dc']))
     {{-- report start --}}
        <li class="nav-item">
           <a href="#" class="nav-link">
          <i class="nav-icon fa fa-file" aria-hidden="true"></i>
          <p>
            রিপোর্ট সংগ্রহ
           <i class="fas fa-angle-left right"></i>
          </p>
        </a>
         <ul class="nav nav-treeview">
           <li class="nav-item ">
               <a href="#" class="nav-link">
                 <i class="nav-icon far fa-user text-info"></i>
                   <p>ইউনিয়নের নাম > ওয়ার্ড> সাধারণ কর</p>
               </a>
           </li>
           <li class="nav-item ">
              <a href="#" class="nav-link">
                <i class="nav-icon far fa-user text-info"></i>
                 <p>ইউনিয়নের নাম> ওয়ার্ড > ব্যবসায় কর</p>
              </a>
           </li>
        </ul>
    </li>
    {{-- report end --}}

      {{-- due list start --}}
      <li class="nav-item">
        <a href="#" class="nav-link">
       <i class="nav-icon fab fa-cc-amazon-pay"></i>
       <p>
         বাকির তালিকা
        <i class="fas fa-angle-left right"></i>
       </p>
     </a>
      <ul class="nav nav-treeview">
        <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-user text-info"></i>
                <p>সাধারণ হোল্ডিং</p>
            </a>
        </li>
        <li class="nav-item">
           <a href="#" class="nav-link">
             <i class="nav-icon far fa-user text-info"></i>
              <p>ব্যবসায় হোল্ডিং</p>
           </a>
        </li>
     </ul>
 </li>
 {{-- due list end --}}

    {{-- sms strt --}}
    <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-sms"></i>
          <p>
            খুদেবার্তা প্রেরণ
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>সব খুদেবার্তা প্রেরণ</p>
            </a>
          </li>
          {{-- <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>ডিএ্যাকটিভ এসএমএস জি অ্যান্ড বি</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>মোবাইল নম্বর ডাউনলোড করুন</p>
            </a>
          </li> --}}
        </ul>
      </li>
    {{-- sms end --}}

     {{-- shit strt --}}
     <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fa fa-file"></i>
          <p>
            শীর্ষ শিট
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>সংগ্রহ (এক নজরে / ইউনিয়ন দ্বারা)</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>বাকি (এক নজরে / ইউনিয়ন দ্বারা)</p>
            </a>
          </li>
        </ul>
      </li>
    {{-- shit end --}}

    {{-- assessment strt --}}
    <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fa fa-file"></i>
          <p>
            মূল্যায়ন ডাউনলোড
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>সাধারণ কর</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>ব্যবসা কর</p>
            </a>
          </li>
        </ul>
      </li>
    {{-- assessment end --}}

      {{-- assessment strt --}}
      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="nav-icon fa fa-file"></i>
          <p>
            নোটিস প্রেরণ
            <i class="fas fa-angle-left right"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>নোটিস</p>
            </a>
          </li>
        </ul>
      </li>
    {{-- assessment end --}}

@endif 