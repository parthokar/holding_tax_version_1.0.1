  <script>
        $('#taxPayerForm').validate({
        rules: {
            name: {
                required: true
            },
            email :{
                required: true
            },
            post_name :{
                required: true
            },
            post_code :{
                required: true
            }
            ,
            holding_no :{
                required: true
            }
            ,
            village :{
                required: true
            }
            ,
            religion :{
                required: true
            }
            ,
            nid :{
                required: true
            },
            home_no :{
                required: true
            }
            ,
            husband_name :{
                required: true
            }
            ,
            fathers_name :{
                required: true
            }
             ,
            room_no :{
                required: true
            }
             ,
            mobile :{
                required: true
            }
             ,
            vumir_poriman :{
                required: true
            }
             ,
            vumir_poriman_shotok :{
                required: true
            }
             ,
            abadi_poriman :{
                required: true
            }
             ,
            family_member :{
                required: true
            }
             ,
            male :{
                required: true
            },
            female :{
                required: true
            },
            tax_value :{
                required: true
            },
            last_tax_year :{
                required: true
            },
            tax_per_year :{
                required: true
            }


        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        }
    });
    </script>
