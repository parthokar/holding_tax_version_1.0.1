@extends('backend.layouts.master')
@section('title','নতুন ব্যবহারকারী')
@section('master_content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('backend.includes.messages')
            <h4 class="text-info">নির্বাচন করুন :</h4>
            <div class="form-group">
                <form id="userForm" method="get" action="{{route('user.form')}}">
                    <select id="formChange" name="select" id="userType" class="form-control" name="type">
                      <option value="" selected>নির্বাচন করুন</option>
                        <option value="1">জেলা প্রশাসক</option>
                        <option value="2">ইউএনও প্রশাসক</option>
                        <option value="3">পৌরসভা প্রশাসক </option>
                        <option value="4">ইউনিয়ন প্রশাসক </option>
                        {{-- <option value="5">পৌরসভা অ্যাক্সেসর </option>
                        <option value="6">ইউনিয়ন অ্যাক্সেসর </option>
                        <option value="7">পৌরসভা কর সংগ্রাহক </option>
                        <option value="8">ইউনিয়ন কর সংগ্রাহক </option>
                   --}}
                    </select>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
 $("#formChange").change(function(){
    $("#userForm").submit();
 });
</script>
@endpush
