@extends('backend.layouts.master')
@section('title','Download mobile no')
@section('master_content')
<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card">
                <div class="card-header d-flex" style="justify-content: space-between">
                    <h3 class="text-primary"> Download mobile no</h3>
                </div>
                <div class="card-body">
                   <div class="row">
                       <div class="col-md-6">
                        <form method="post" action="{{route('download.mobile.union')}}">
                            @csrf 
                            <select class="form-control" name="union_id" required>
                                <option value="">Select Union</option>
                                @foreach($unionPorishod as $item)  
                                <option value="{{$item->id}}">{{$item->union_porishod_name}}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="mt-3 btn btn-success"> <i class="fa fa-download"></i></button>
                         </form>
                       </div>
                       <div class="col-md-6">
                        <form method="post" action="{{route('download.mobile.word')}}">
                            @csrf 
                          <select class="form-control" name="word_id" required>
                             <option value="">Select Ward</option>
                             @foreach($word as $item)  
                               <option value="{{$item->id}}">{{$item->word_name}}</option>
                             @endforeach
                          </select>
                          <button type="submit" class="mt-3 btn btn-success"><i class="fa fa-download"></i></button>
                        </form>
                      </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

