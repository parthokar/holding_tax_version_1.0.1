@extends('backend.layouts.master')
@section('title','Send Sms')
@section('master_content')
<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="card">
                @include('backend.includes.messages')
                <div class="card-header d-flex" style="justify-content: space-between">
                    <h3 class="text-primary"> বার্তা পাঠান</h3>
                </div>
                <div class="card-body">
                   <div class="row">
                    <div class="col-md-4">
                        <form method="post" action="{{route('all.business.general')}}">
                            @csrf 
                           <div class="form-group"> 
                                <select class="form-control" name="word_id">
                                        <option value="">সমস্ত সাধারণ ও ব্যবসায়িক ব্যবহারকারী</option>
                                </select>
                            </div>
                            <div class="form-group"> 
                                <label>বার্তা</label>
                                <textarea class="form-control" name="message" required></textarea>
                            </div>
                          <button type="submit" class="btn btn-success">বার্তা পাঠান</button>
                        </form>
                      </div>
                       <div class="col-md-4">
                        <form method="post" action="{{route('all.general')}}">
                            @csrf 
                            <div class="form-group"> 
                                <select class="form-control" name="union_id">
                                    <option value="">সাধারণ হোল্ডিং ব্যবহারকারী</option>
                                </select>
                             </div>
                            <div class="form-group"> 
                                <label>বার্তা</label>
                                <textarea class="form-control" name="message" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-success">বার্তা পাঠান</button>
                         </form>
                       </div>
                       <div class="col-md-4">
                        <form method="post" action="{{route('all.business')}}">
                            @csrf 
                            <div class="form-group"> 
                                <select class="form-control" name="union_id">
                                    <option value="">ব্যবসায় হোল্ডিং ব্যবহারকারী</option>
                                </select>
                            </div>
                            <div class="form-group"> 
                                <label>বার্তা</label>
                                <textarea class="form-control" name="message" required></textarea>
                            </div> 
                            <button type="submit" class="btn btn-success">বার্তা পাঠান</button>
                        </form>
                      </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

