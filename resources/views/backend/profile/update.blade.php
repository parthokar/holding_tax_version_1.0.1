@extends('backend.layouts.master')

@section('title','প্রোফাইল পরিবর্তন')
@section('master_content')
<div class="container-fluid">
    <h3 class="text-info">প্রোফাইল পরিবর্তন করুন</h3>
    <hr>
    <form action="{{ url("update/profile/$data->id/$auth->user_type") }}" method="post" enctype="multipart/form-data">
        @csrf
    <div class="row">
        <div class="col-12 col-md-4">
            <div class="form-group">
                <label for="">প্রথম নাম :</label>
                <input type="text" class="form-control" name="first_name" value="{{ $auth->first_name }}">
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="form-group">
                <label for="">শেষ নাম : </label>
                <input type="text" class="form-control" name="last_name" value="{{ $auth->last_name }}">
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="form-group">
                <label for="">মোবাইল : </label>
                <input type="text" class="form-control" name="mobile_no" value="{{ $data->mobile_no }}">
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="form-group">
                <label for="">লোগো : </label>
                 <input type="file" name="office_logo">
                 @if ($data->office_logo)
                    <img src="" alt="">
                @endif
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="form-group">
                <label for="">ফটো : </label>
                <input type="file" name="image">
                @if ($data->image)
                    <img src="" alt="">
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <button class="btn btn-block btn-success">হালনাগাদ কর</button>
        </div>
    </div>
    </form>
</div>
@stop
