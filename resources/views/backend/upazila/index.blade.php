@extends('backend.layouts.master')
@section('title','উপজেলা')
@push('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush
@section('master_content')
<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header d-flex ">
                    <h3 class="text-primary"> উপজেলা</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>উপজেলা</th>
                                    <th>জেলা</th>
                                    <th>অবস্থা</th>
                                    <th>আরও</th>
                                </tr>
                            </thead>
                            <tbody id="upazilaTable"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    <div class="col-12 col-md-4">
        <div class="card">
            <div class="card-header">
                <h3 class="text-primary">নতুন উপজেলা</h3>
            </div>
            <div class="card-body">
                <form action="{{ route('upazila.store') }}" id="addUdpazilaFor" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">জেলা নির্বাচন করুন</label>
                        <select name="district_id" id="distirct_id" class="form-control select2" required>
                            <option value="">জেলা নির্বাচন করুন</option>
                            @forelse ($districts as $district)
                            <option value="{{ $district->id }}">{{ $district->name }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="input-group mb-3 dynamic_file">
                         <input type="text" class="form-control" placeholder="উপজেলার নাম" id="name" name="upazila[]" required>
                        <div class="input-group-append">
                            <button class="btn btn-sm btn-success add_more_file_btn" id="addMoreBtn" ><i class="fa fa-plus-circle" ></i></button>
                        </div>
                        </div>
                        <div class="dynamic_upazila_add mt-2" style="display: none">
                        <div class="input-group form-group">
                            <input type="text" class="form-control" name="upazila[]"value="" placeholder="উপজেলার নাম">
                            <button class="btn btn-danger btn-sm delete_more_file_btn" type="button"><i class="fa fa-minus-circle"></i></button>
                            </div>
                        </div>
                        <!--  <div class="form-group">
                        <label for=""> Name : </label>
                        <span id="nameError" class="text-danger"></span>
                        </div> -->

                         <div class="form-group">
                    <button class="btn btn-block btn-success" type="submit"><i class="fa fa-plus mr-1"></i> উপজেলা</button>
                 </div>
            </form>
            </div>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="editModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">আপডেট উপজেলা</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                 <form action="" id="updateUpazilaForm">
                        <div class="form-group">
                            <label for="">জেলা নির্বাচন করুন</label>
                        <select name="district_id" id="e_distirct_id" class="form-control  " required>
                            <option value="">জেলা নির্বাচন করুন</option>
                            @forelse ($districts as $district)
                            <option value="{{ $district->id }}">{{ $district->name }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                        <div class="form-group">
                            <label for=""> নাম : </label>
                            <input type="text" class="form-control" placeholder="Enter Country Name" id="e_name">
                            <span id="nameError" class="text-danger"></span>
                        </div>
                         <div class="form-group">
                             <label for="">অবস্থা :</label>
                            <select name="" id="e_status" class="form-control">
                                <option value="1">সক্রিয়</option>
                                <option value="0">নিষ্ক্রিয়</option>
                            </select>
                        </div>
                         <input type="hidden" id="e_id">
                         <div class="form-group">
                            <button class="btn btn-block btn-success" id="">আপডেট উপজেলা</button>
                        </div>
                    </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

@stop

@push('script')

<script src="{{ asset('backend') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script>

    $('body').on('click','.add_more_file_btn',function (e) {
        e.preventDefault()
        var html = $('.dynamic_upazila_add').html();
        $('.dynamic_file').after(html);
    });
    //remove dynamic certificate
    $('body').on('click','.delete_more_file_btn',function (e) {
        e.preventDefault()
        $(this).parent('.input-group').remove();
    });
    $('.select2').select2()
</script>
<script>
        function table_data_row(data) {
            var	rows = '';
            var i = 0;
            $.each( data, function( key, value ) {
                value.id
                rows += '<tr>';
                rows += '<td>'+ ++i +'</td>';
                rows += '<td>'+value.name+'</td>';
                rows += '<td>'+value.district.name+'</td>';
                rows += '<td>'
                if(value.status == 0) {
                    rows += '<span class="badge badge-warning">নিষ্ক্রিয়</span>'
                }else if(value.status == 1){
                    rows += '<span class="badge badge-success">সক্রিয়</span>'
                }
                rows += '</td>'
                rows += '<td data-id="'+value.id+'" class="text-center">';
                rows += '<a class="btn btn-sm btn-info text-light" id="editRow" data-id="'+value.id+'" data-toggle="modal" data-target="#editModal">আপডেট</a> ';
                rows += '</td>';
                rows += '</tr>';
            });
            $("#upazilaTable").html(rows);
    }
        function getAllUpazila(){
            axios.get("{{ route('get-all-upazila') }}")
            .then(function(res){
               //console.log(res.data);
                table_data_row(res.data);
              $('.dataTable').DataTable();
            })
        }
        getAllUpazila();

        //store
         $('body').on('submit','#addUpazilaForm',function(e){
        e.preventDefault();
        let nameError = $('#nameError')

        axios.post("{{ route('upazila.store') }}", {
            name: $('#name').val()
        })
        .then(function (response) {
            getAllUpazila();
             $('#name').val('')
          // console.log(response.data)
              setSwalMessage()
          $('#addModal').modal('toggle')
        })
        .catch(function (error) {
            if(error.response.data.errors.name){
                nameError.text(error.response.data.errors.name[0]);
            }
            console.log(error);
        });
    });



 //edit
    $('body').on('click','#editRow',function(){
        let id = $(this).data('id')
        let url = base_path + '/upazila' + '/'  + id + '/edit'
       // console.log(url);
        axios.get(url)
            .then(function(res){
                //console.log(res.data);
                $('#e_distirct_id').val(res.data.district_id)
                $('#e_name').val(res.data.name)
                $('#e_id').val(res.data.id)
                $('#e_status').val(res.data.status)
            })
    })

      //update
    $('body').on('submit','#updateUpazilaForm',function(e){
        e.preventDefault()
        let id = $('#e_id').val()
        let data = {
            id : id,
            name : $('#e_name').val(),
            district_id : $('#e_distirct_id').val(),
            status : $('#e_status').val()
        }
        let url = base_path + '/upazila' + '/'  + id
        axios.put(url,data)
        .then(function(res){
            getAllUpazila();
             $('#editModal').modal('toggle')
            setSwalMessage('success','Success','সফলভাবে তৈরি করা হয়েছে!');
            //console.log(res);
        })
    })
    </script>
@endpush

