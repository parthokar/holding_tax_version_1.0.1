@extends('backend.layouts.master')
@section('title','Create Dc')
@section('master_content')
    <div class="card card-info card-outline">
        <div class="card-header">
            <h3 class="card-title">ডি.সি. অ্যাকাউন্ট তৈরি করুন</h3>
        </div>
        <form role="form" method="post"  action="{{route('dc.store')}}" enctype="multipart/form-data">@csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">জেলা</label>
                            <select class="selectpicker form-control" name="district_id" id="district" data-live-search="true">
                                @foreach($districts as $district)
                                    <option value="{{$district->id}}">{{$district->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">পদবি</label>
                            <select class="selectpicker form-control" name="designation_id" id="designation" data-live-search="true">
                                @foreach($designations as $designation)
                                    <option value="{{$designation->id}}">{{$designation->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">নাম</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">মোবাইল</label>
                            <input type="number" class="form-control" name="mobile" id="mobile" placeholder="Enter mobile">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">ইমেল</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">পাসওয়ার্ড</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">পাসওয়ার্ড নিশ্চিত করুন</label>
                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Password">
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-info float-right">Save</button>
            </div>
        </form>
    </div>
@stop
