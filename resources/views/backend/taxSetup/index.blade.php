@extends('backend.layouts.master')
@section('title','কর ফি সেটআপ')
@push('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush
@section('master_content')
<div class="container-fluld">
    <div class="row">
        <div class="col-12 col-md-8">
            <div class="card">
                @include('backend.includes.messages')
                <div class="card-header">
                    <h3 class="text-info">কর ফি সেটআপ</h3>
                </div>
                <div class="card-body">
                     <div class="table-responsive">
                        <table id="myTable" class="table table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr style="font-size: 14px">
                                    <th>নং</th>
                                    <th>বাড়ি</th>
                                    <th>কক্ষ</th>
                                    <th>চাকরি</th>
                                    <th>কর</th>
                                    <th>তৈরি করেছেন</th>
                                    <th>অবস্থা</th>
                                    <th>আরও</th>
                                </tr>
                            </thead>
                             <tbody>
                            @foreach($tax as $key=>$item)
                              <tr>
                                  <td>{{++$key}}</td>
                                  <td>{{$item->house->house_name}}</td>
                                  <td>{{$item->room->room_name}}</td>
                                  <td>{{$item->job->job_type}}</td>
                                  <td>{{$item->tax_amount}}</td>
                                  <td>{{$item->user->first_name}} {{$item->user->last_name}}</td>
                                  <td>@if($item->status==1) সক্রিয় @else নিষ্ক্রিয় @endif</td>
                                  <td>
                                      <a href="{{route('taxsetup.edit',$item->id)}}" class="btn btn-sm btn-info text-light">Edit</a>
                                  </td>
                              </tr>
                            @endforeach 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4" id="addPortion">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-info">নতুন কর ফি সেটআপ</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('taxsetup.store') }}" id="addUnionFor" method="post">
                        @csrf
                        <div class="form-group">
                          <label>বাড়ি নির্বাচন করুন</label>  
                          <select class="form-control" name="house_id">
                              @foreach($house as $houses)
                                <option value="{{$houses->id}}">{{$houses->house_name}}</option>
                              @endforeach
                          </select>
                        </div>
                        @if($errors->has('house_id'))
                        <div class="error">
                            {{$errors->first('house_id')}}
                        </div>
                        @endif
                        <div class="form-group">
                            <label>রুম নির্বাচন করুন</label>  
                            <select class="form-control" name="room_id">
                                @foreach($room as $rooms)
                                  <option value="{{$rooms->id}}">{{$rooms->room_name}}</option>
                                @endforeach
                            </select>
                          </div>
                          @if($errors->has('room_id'))
                          <div class="error">
                              {{$errors->first('room_id')}}
                          </div>
                          @endif
                          <div class="form-group">
                            <label>পেশা নির্বাচন করুন</label>  
                            <select class="form-control" name="job_id">
                                @foreach($job as $jobs)
                                   <option value="{{$jobs->id}}">{{$jobs->job_type}}</option>
                                @endforeach
                            </select>
                          </div>
                          @if($errors->has('job_id'))
                          <div class="error">
                              {{$errors->first('job_id')}}
                          </div>
                          @endif
                          <div class="form-group">
                            <label>Tax</label>  
                            <input type="text" class="form-control" name="tax_amount"> 
                          </div>
                          @if($errors->has('tax_amount'))
                          <div class="error">
                              {{$errors->first('tax_amount')}}
                          </div>
                          @endif
                         <div class="form-group">
                           <button class="btn btn-block btn-success" id="addButton">সংরক্ষণ করুন</button>
                        </div>
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script src="{{ asset('backend') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script>
      $('#myTable').DataTable();
</script>
@endpush

