@extends('backend.layouts.master')
@section('title','নতুন ব্যবহারকারী')
@section('master_content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('backend.includes.messages')
            <h4 class="text-info">নির্বাচন করুন :</h4>
            <div class="form-group">
                <form id="userForm" method="get" action="{{route('user.form')}}">
                    <select id="formChange" name="select" id="userType" class="form-control" name="type">
                        <option value="">নির্বাচন করুন</option>
                        <option value="1">জেলা প্রশাসক</option>
                        <option value="2">ইউএনও প্রশাসক</option>
                        <option value="3">পৌরসভা প্রশাসক </option>
                        <option value="4" >ইউনিয়ন প্রশাসক </option>
                        <!--
                             <option value="5">পৌরসভা অ্যাক্সেসর </option>
                        <option value="6">ইউনিয়ন অ্যাক্সেসর </option>
                        <option value="7" selected>পৌরসভা কর সংগ্রাহক </option>
                        <option value="8">ইউনিয়ন কর সংগ্রাহক </option>
                         -->

                    </select>
                </form>
            </div>
        </div>

        {{-- form --}}
        <div class="col-md-12">
            <h3 class="text-info text-center">পৌরসভা কর সংগ্রাহক </h3>
            <form action="{{route('user.store',8)}}" method="post" id="f7">
                @csrf
                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">জেলা নির্বাচন করুন : </label>
                            <select name="zilla_id" id="dc_district_id" class="form-control" required>
                                <option value="">Select</option>
                                @foreach($zilla as $zillas)
                                   <option value="{{$zillas->id}}">{{$zillas->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for=""> পৌরসভা নির্বাচন করুন : </label>
                            <select name="pouroshova_id" id="pouro_puroshova_id" class="form-control" required>
                                <option value="">Select</option>
                                @foreach($pouroshova as $pouroshovas)
                                  <option value="{{$pouroshovas->id}}">{{$pouroshovas->pouroshova_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">ওয়ার্ড নির্বাচন করুন : </label>
                            <select name="word_id" id="pouro_puroshova_id" class="form-control" required>
                                <option value="">Select</option>
                                 @foreach($word as $words)
                                   <option value="{{$words->id}}">{{$words->word_name}}</option>
                                 @endforeach
                            </select>
                        </div>
                    </div>

                     <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">উপাধি নির্বাচন করুন : </label>
                            <select name="designation_id" id="dc_designation_id" class="form-control" required>
                                <option value="">Select</option>
                                @foreach($designation as $designations)
                                   <option value="{{$designations->id}}">{{$designations->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="from-group">
                            <label for="">পোস্ট কোড : </label>
                            <input type="text" class="form-control" name="post_code" placeholder="Post Code" id="pouro_accesor_postCode" required>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="from-group">
                            <label for=""> প্রথম নাম   : </label>
                            <input type="text" class="form-control" name="first_name" placeholder="First name" id="pouro_accesor_firstName" required>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="from-group">
                            <label for="">শেষ নাম : </label>
                            <input type="text" class="form-control" name="last_name" placeholder="Last name" id="pouro_accesor_lastName" required>
                        </div>
                    </div>
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">ফোন : </label>
                            <input type="text" class="form-control" name="mobile_no" placeholder="Phone Number" id="pouro_accesor_phoneNumber" required>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label for="">ইমেল : </label>
                            <input type="email" name="email" class="form-control" placeholder="email Number" id="e7" required>
                            <span id="dc_email_id_error" class="text-danger"></span>
                        </div>
                    </div>
                      <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label for="">পাসওয়ার্ড : </label>
                            <input type="password" name="password" class="form-control" placeholder="password Number" id="dc_password" required>
                            <span id="dc_password_id_error" class="text-danger"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <button id="submit7" type="submit" class="btn btn-block btn-sm btn-success">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        {{-- form --}}
    </div>
</div>
@endsection

@push('script')
<script>
 $("#formChange").change(function(){
    $("#userForm").submit();
 });
</script>
<script>
    $('#f7').validate({
        rules: {
            name: {
                required: true
            },
            email :{
                required: true
            },
            subject :{
                required: true
            },
            text :{
                required: true
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        }
    });
</script>
@endpush
