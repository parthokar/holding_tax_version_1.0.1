@extends('backend.layouts.master')
@section('title','নতুন ব্যবহারকারী')
@section('master_content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('backend.includes.messages')
            <h4 class="text-info">নির্বাচন করুন :</h4>
            <div class="form-group">
                <form id="userForm" method="get" action="{{route('user.form')}}">
                    <select id="formChange" name="select" id="userType" class="form-control" name="type">
                         <option value="">নির্বাচন করুন</option>
                        <option value="1" selected>জেলা প্রশাসক</option>
                        <option value="2">ইউএনও প্রশাসক</option>
                        <option value="3">পৌরসভা প্রশাসক </option>
                        <option value="4">ইউনিয়ন প্রশাসক </option>
                        <!--
                            <option value="5">পৌরসভা অ্যাক্সেসর </option>
                        <option value="6">ইউনিয়ন অ্যাক্সেসর </option>
                        <option value="7">পৌরসভা কর সংগ্রাহক </option>
                        <option value="8">ইউনিয়ন কর সংগ্রাহক </option>
                         -->

                    </select>
                </form>
            </div>
        </div>

        {{-- form --}}
        <div class="col-md-12">
        <h3 class="text-info text-center">নতুন জেলা প্রশাসক </h3>
        <hr>
        <div class="card">
            <div class="card-header text-center">
            <h3 class="text-light text-center bg-cyan d-inline-block p-3">ডিজিটাল সেন্টার সম্পর্কিত তথ্য</h3>
            </div>
            <div class="card-body">
                 <form action="{{route('user.store',2)}}" method="post" id="f1">
            @csrf
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="">জেলা নির্বাচন করুন : </label>
                        <select name="zilla_id" id="dc_district_id" class="form-control" required>
                            <option value="">নির্বাচন করুন</option>
                            @foreach($zilla as $zillas)
                               <option value="{{$zillas->id}}">{{$zillas->name}}</option>
                            @endforeach
                        </select>
                        <span id="dc_district_id_error" class="text-danger"></span>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="">উপাধি নির্বাচন করুন : </label>
                        <select name="designation_id" id="dc_designation_id" class="form-control" required>
                            <option value="">নির্বাচন করুন</option>
                            @foreach($designation as $designations)
                               <option value="{{$designations->id}}">{{$designations->name}}</option>
                            @endforeach
                        </select>
                        <span id="dc_designation_id_error" class="text-danger"></span>
                    </div>
                </div>
            </div>
            <div class="card-header text-center">
                <h3 class="text-center bg-cyan d-inline-block p-3">প্রশাসক সম্পর্কিত তথ্য</h3>
            </div>
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="from-group">
                        <label for="">প্রথম নাম : </label>
                        <input type="text" value="{{old('first_name')}}" class="form-control" name="first_name" placeholder="প্রথম নাম" id="dc_firstName" required value="{{ old('first_name') }}">
                        <span id="dc_f_name_id_error" class="text-danger"></span>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="from-group">
                        <label for="">শেষ নাম : </label>
                        <input type="text" class="form-control" name="last_name" placeholder="শেষ নাম" id="dc_lastName" required value="{{ old('last_name') }}">
                    </div>
                </div>
                 <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="">ফোন : </label>
                        <input type="text" name="mobile_no" class="form-control" placeholder="ফোন" id="dc_phoneNumber" required value="{{ old('mobile_no') }}">
                        <span id="dc_phone_id_error" class="text-danger"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="">ইমেল : </label>
                        <input type="text" name="email" class="form-control" placeholder="ইমেল" id="e1" required value="{{ old('email') }}">
                        <span id="dc_email_id_error" class="text-danger"></span>
                    </div>
                </div>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="">পাসওয়ার্ড : </label>
                        <input type="password" name="password" class="form-control" placeholder=" পাসওয়ার্ড" id="dc_password" required>
                        <span id="dc_password_id_error" class="text-danger"></span>
                    </div>
                </div>
                 <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="">এন আই ডি
 নং : </label>
                        <input type="text" name="nid" class="form-control" placeholder="এন আই ডি
 নং" id="nid" required>
                        <span id="dc_nid_id_error" class="text-danger"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <button id="submit1" type="submit" class="btn btn-block btn-sm btn-info">সাবমিট</button>
                    </div>
                </div>
            </div>
        </form>
            </div>
        </div>

        </div>
        {{-- form --}}
    </div>
</div>
@endsection

@push('script')
<script>
 $("#formChange").change(function(){
    $("#userForm").submit();
 });

</script>
<script>
    $('#f1').validate({
        rules: {
            name: {
                required: true
            },
            email :{
                required: true
            },
            subject :{
                required: true
            },
            text :{
                required: true
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        }
    });
</script>
@endpush
