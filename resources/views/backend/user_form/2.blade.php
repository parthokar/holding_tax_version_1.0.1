@extends('backend.layouts.master')
@section('title','নতুন ব্যবহারকারী')
@section('master_content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @include('backend.includes.messages')
            <h4 class="text-info">নির্বাচন করুন :</h4>
            <div class="form-group">
                <form id="userForm" method="get" action="{{route('user.form')}}">
                    <select id="formChange" name="select" id="userType" class="form-control" name="type">
                         <option value="">নির্বাচন করুন</option>
                        <option value="1">জেলা প্রশাসক</option>
                        <option value="2" selected>ইউএনও প্রশাসক</option>
                        <option value="3">পৌরসভা প্রশাসক </option>
                        <option value="4" >ইউনিয়ন প্রশাসক </option>
                        <!--
 <option value="5">পৌরসভা অ্যাক্সেসর </option>
                        <option value="6">ইউনিয়ন অ্যাক্সেসর </option>
                        <option value="7">পৌরসভা কর সংগ্রাহক </option>
                        <option value="8">ইউনিয়ন কর সংগ্রাহক </option>
                         -->

                    </select>
                </form>
            </div>
        </div>

        {{-- form --}}
        <div class="col-md-12">

            <h3 class="text-info text-center">নতুন ইউনো প্রশাসক </h3>
            <hr>
                   <form action="{{route('user.store',3)}}" method="post" id="f2">
                       @csrf
                       <div class="row">
                           <div class="col-12 col-md-3">
                               <div class="form-group">
                                   <label for="">জেলা নির্বাচন করুন : </label>
                                   <select name="zilla_id" id="zilla_id" class="form-control" required>
                                       <option value="">নির্বাচন করুন</option>
                                       @foreach($zilla as $zillas)
                                          <option value="{{$zillas->id}}">{{$zillas->name}}</option>
                                       @endforeach
                                   </select>
                                   <span id="dc_district_id_error" class="text-danger"></span>
                               </div>
                           </div>
                           <div class="col-12 col-md-3">
                               <div class="form-group">
                                   <label for="">উপজেলা নির্বাচন করুন : </label>
                                   <select name="upazilla_id" id="sub" class="form-control" required>
                                       <option value="">নির্বাচন করুন</option>
                                   </select>
                                    <span id="uno_upazila_id_error" class="text-danger"></span>
                               </div>
                           </div>
                           <div class="col-12 col-md-3">
                               <div class="form-group">
                                   <label for="">উপাধি নির্বাচন করুন : </label>
                                   <select name="designation_id" id="dc_designation_id" class="form-control" required>
                                       <option value="">নির্বাচন করুন</option>
                                       @foreach($designation as $designations)
                                          <option value="{{$designations->id}}">{{$designations->name}}</option>
                                       @endforeach
                                   </select>
                                   <span id="dc_designation_id_error" class="text-danger"></span>
                               </div>
                           </div>
                           <div class="col-12 col-md-3">
                               <div class="from-group">
                                   <label for="">পোস্ট কোড  : </label>
                                   <input type="text" class="form-control" name="post_code" placeholder="পোস্ট কোড " id="uno_postCode" required>
                                    <span id="uno_post_id_error" class="text-danger"></span>
                               </div>
                           </div>
                       </div>
  <div class="card-header text-center">
                <h3 class="text-center bg-cyan d-inline-block p-3">প্রশাসক সম্পর্কিত তথ্য</h3>
            </div>
                       <div class="row">
                           <div class="col-12 col-md-4">
                               <div class="from-group">
                                   <label for="">প্রথম নাম : </label>
                                   <input type="text" class="form-control" name="first_name" placeholder="প্রথম নাম" id="uno_firstName" required>
                                    <span id="uno_fitst_name_error" class="text-danger"></span>
                               </div>
                           </div>
                           <div class="col-12 col-md-4">
                               <div class="from-group">
                                   <label for="">শেষ নাম: </label>
                                   <input type="text" class="form-control" name="last_name" placeholder="শেষ নাম" id="uno_lastName" required>
                                   <span id="uno_last_name_error" class="text-danger"></span>
                               </div>
                           </div>
                            <div class="col-12 col-md-4">
                               <div class="form-group">
                                   <label for="">ফোন : </label>
                                   <input type="text" class="form-control" name="mobile_no" placeholder="ফোন" id="uno_phoneNumber" required>
                                   <span id="uno_phone_error" class="text-danger"></span>
                               </div>
                           </div>
                           <div class="col-12 col-md-4">
                               <div class="form-group">
                                   <label for="">ইমেল : </label>
                                   <input type="text" class="form-control" name="email" placeholder="ইমেল" id="e2" required>
                                   <span id="dc_email_id_error" class="text-danger"></span>
                               </div>
                           </div>
                             <div class="col-12 col-md-4">
                               <div class="form-group">
                                   <label for="">পাসওয়ার্ড : </label>
                                   <input type="text" class="form-control" name="password" placeholder="পাসওয়ার্ড" id="dc_password" required>
                                   <span id="dc_password_id_error" class="text-danger"></span>
                               </div>
                           </div>
                                  <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="">এন আই ডি
 নং : </label>
                        <input type="text" name="nid" class="form-control" placeholder="এন আই ডি
 নং" id="nid" required>
                        <span id="dc_nid_id_error" class="text-danger"></span>
                    </div>
                </div>
                       </div>

                       <div class="row">
                           <div class="col-12">
                               <div class="form-group">
                                   <button id="submit2" type="submit" class="btn btn-block btn-sm btn-info">সাবমিট</button>
                               </div>
                           </div>
                       </div>
                   </form>
             </div>
        {{-- form --}}
    </div>
</div>
@endsection

@push('script')
<script>
 $("#formChange").change(function(){
    $("#userForm").submit();
 });

 $("#zilla_id").change(function(){
    let id=$("#zilla_id").val();
                $.ajax({
                url: "{{url('/user/zilla/')}}" + '/' + id,
                type: "GET",
                success: function(response) {
                    console.log(response);
                    var items = "";
                    items += "<option value=''>উপজেলা নির্বাচন করুন</option>";
                    $.each(response, function(i, item) {
                        items += "<option value='" + item.id + "'>" + (item
                                .name) +
                            "</option>";
                    });
                    $("#sub").html(items);
                 },
                    error: function(response) {
                        console.log(response);
               },
        });
 });
</script>
<script>
    $('#f2').validate({
        rules: {
            name: {
                required: true
            },
            email :{
                required: true
            },
            subject :{
                required: true
            },
            text :{
                required: true
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        }
    });
</script>
@endpush
