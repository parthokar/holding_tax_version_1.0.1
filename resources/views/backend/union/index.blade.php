@extends('backend.layouts.master')
@section('title','ইউনিয়ন')
@push('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="{{ asset('backend') }}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush
@section('master_content')
<div class="container-fluld">
    <div class="row">
        <div class="col-12 col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-info">ইউনিয়ন</h3>
                </div>
                <div class="card-body">
                     <div class="table-responsive">
                        <table class="table table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>নং</th>
                                    <th>নাম</th>
                                    <th>উপজেলা</th>
                                    <th>জেলা</th>
                                    <th>অবস্থা</th>
                                    <th>আরও</th>
                                </tr>
                            </thead>
                            <tbody id="unionTable"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4" id="addPortion">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-info">নতুন ইউনিয়ন</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('union.store') }}" id="addUnionFor" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="">জেলা : </label>
                        <select name="district_id" id="distirct_id" class="form-control select2" required>
                            <option value="">জেলা নির্বাচন করুন</option>
                            @forelse ($districts as $district)
                            <option value="{{ $district->id }}">{{ $district->name }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">উপজেলা : </label>
                        <select name="upazila_id" id="upazila_id_select" class="form-control select2" required>
                            <option value="">উপজেলা নির্বাচন করুন</option>
                        </select>
                    </div>
                    <label for="">ইউনিয়ন</label>
                        <div class="input-group mb-3 dynamic_file">
                         <input type="text" class="form-control" placeholder="ইউনিয়ন নাম" id="name" name="union[]" required>
                        <div class="input-group-append">
                            <button class="btn btn-sm btn-success add_more_file_btn" id="addMoreBtn" ><i class="fa fa-plus-circle" ></i></button>
                        </div>
                        </div>
                        <div class="dynamic_union_add mt-2" style="display: none">
                        <div class="input-group form-group">
                            <input type="text" class="form-control" name="union[]"value="" placeholder="ইউনিয়ন নাম">
                            <button class="btn btn-danger btn-sm delete_more_file_btn" type="button"><i class="fa fa-minus-circle"></i></button>
                            </div>
                        </div>
                         <div class="form-group">
                    <button class="btn btn-block btn-success" id="addButton">নতুন ইউনিয়ন</button>
                 </div>
                </form>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4" id="editPortion">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-info">হালনাগাদ ইউনিয়ন</h3>
                </div>
                <div class="card-body">
                     <form action="" id="updateUnionForm">
                          <div class="form-group">
                            <label for="">জেলা : </label>
                        <select name="district_id" id="e_distirct_id" class="form-control " required>
                            <option value="">জেলা নির্বাচন করুন</option>
                            @forelse ($districts as $district)
                            <option value="{{ $district->id }}">{{ $district->name }}</option>
                            @empty
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">উপজেলা : </label>
                        <select name="upazila_id" id="e_upazila_id_select" class="form-control  edit_upazila" required>
                            <option value="">উপজেলা নির্বাচন করুন</option>
                        </select>
                    </div>
                        <div class="form-group">
                            <label for=""> নাম : </label>
                            <input type="text" class="form-control" placeholder="Enter Country Name" id="e_name">
                            <span id="nameError" class="text-danger"></span>
                        </div>
                            <div class="form-group">
                                <label for="">অবস্থা :</label>
                            <select name="" id="e_status" class="form-control">
                                <option value="1">সক্রিয়</option>
                                <option value="0">নিষ্ক্রিয়</option>
                            </select>
                        </div>
                         <input type="hidden" id="e_id">
                         <div class="form-group">
                            <button class="btn btn-block btn-success" id="addButton">আপডেট উপজেলা</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('script')
<script src="{{ asset('backend') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('backend') }}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script>
    $('.select2').select2()
    function table_data_rows_for_select_option(data) {
        let html = '<option value="">উপজেলা নির্বাচন করুন</option>'
         $.each( data, function( key, value ) {
            html += '<option value="'+ value.id+'">'+ value.name +'</option>'
        })
        //console.log(html);
        return html
    }
    //dynamic change option
    $('#distirct_id ').on('change',function(){
        let id = $(this).val()
        axios.get(`${base_path}/get-upazila-by-district-id/${id}`)
        .then((response) => {
            //console.log(response.data);
            //console.log(table_data_rows_for_select_option(response.data));
            $('#upazila_id_select').html(table_data_rows_for_select_option(response.data))
        })
    });
      $('#e_distirct_id ').on('change',function(){
        let id = $(this).val()
        axios.get(`${base_path}/get-upazila-by-district-id/${id}`)
        .then((response) => {
            //console.log(response.data);
            //console.log(table_data_rows_for_select_option(response.data));
            $('#e_upazila_id_select').html(table_data_rows_for_select_option(response.data))
        })
    });
      $('#e_distirct_id ').on('change',function(){
        let id = $(this).val()
        axios.get(`${base_path}/get-upazila-by-district-id/${id}`)
        .then((response) => {
            console.log(response.data);
            //console.log(table_data_rows_for_select_option(response.data));
            $('#upazila_id_select').html(table_data_rows_for_select_option(response.data))
        })
    });
    //dynamic value field
    $('body').on('click','.add_more_file_btn',function (e) {
        e.preventDefault()
        var html = $('.dynamic_union_add').html();
        $('.dynamic_file').after(html);
    });
    //remove dynamic certificate
    $('body').on('click','.delete_more_file_btn',function (e) {
        e.preventDefault()
        $(this).parent('.input-group').remove();
    });

</script>
<script>

    let edit = $('#editPortion')
    let add = $('#addPortion')
    edit.hide()
    function table_data_row(data) {
            var	rows = '';
            var i = 0;
            $.each( data, function( key, value ) {
                value.id
                rows += '<tr>';
                rows += '<td>'+ ++i +'</td>';
                rows += '<td>'+value.name+'</td>';
                 rows += '<td>'+value.upazila.name+'</td>';
                rows += '<td>'+value.district.name+'</td>';
                  rows += '<td>'
                if(value.status == 0) {
                    rows += '<span class="badge badge-warning">নিষ্ক্রিয়</span>'
                }else if(value.status == 1){
                    rows += '<span class="badge badge-success">সক্রিয়</span>'
                }
                rows += '</td>'
                rows += '<td data-id="'+value.id+'" class="text-center">';
                rows += '<a class="btn btn-sm btn-info text-light" id="editRow" data-id="'+value.id+'" data-toggle="modal" data-target="#editModal">আপডেট</a> ';
                rows += '</td>';
                rows += '</tr>';
            });
            $("#unionTable").html(rows);
    }
        function getAllUnion(){
            axios.get("{{ route('get-all-union') }}")
            .then(function(res){
               //console.log(res);
                table_data_row(res.data);
              $('.dataTable').DataTable();
            })
        }
        getAllUnion();

        //store
         $('body').on('submit','#addUnionForm',function(e){
        e.preventDefault();
        let nameError = $('#nameError')

        axios.post("{{ route('union.store') }}", {
            name: $('#name').val()
        })
        .then(function (response) {
            getAllUnion();
             $('#name').val('')
             $('#nameError').val('')
          // console.log(response.data)
              setSwalMessage()
          $('#addModal').modal('toggle')
        })
        .catch(function (error) {
            if(error.response.data.errors.name){
                nameError.text(error.response.data.errors.name[0]);
            }
            console.log(error);
        });
    });


 //edit
    $('body').on('click','#editRow',function(){
        edit.show()
        add.hide()
        let id = $(this).data('id')
        let url = base_path + '/union' + '/'  + id + '/edit'
       // console.log(url);
        axios.get(url)
            .then(function(res){
                $('#e_name').val(res.data.name)
                $('#e_distirct_id').val(res.data.district_id)
                $('#e_id').val(res.data.id)
                $('#e_status').val(res.data.status)
            })
    })

      //update
    $('body').on('submit','#updateUnionForm',function(e){
        e.preventDefault()
        let id = $('#e_id').val()
        let data = {
            id : id,
            name : $('#e_name').val(),
            upazila_id :   $('#e_upazila_id_select').val(),
            district_id :   $('#e_distirct_id').val(),
            status :   $('#e_status').val()
        }
        let url = base_path + '/union' + '/'  + id
        axios.put(url,data)
        .then(function(res){
            getAllUnion();
             $('#editModal').modal('toggle')
            setSwalMessage('success','Success','সফলভাবে তৈরি করা হয়েছে!');
            edit.hide()
            add.show()
        })
    })
    </script>
@endpush

