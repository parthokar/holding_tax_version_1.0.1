@extends('backend.layouts.master')

@section('title','নতুন করদাতা')
@section('master_content')
<div class="container">
    <h3 class="text-center">নতুন করদাতা তৈরি ফর্ম</h3>
    <hr>
       <div class="form-group">
                <form id="userForm" method="get" action="{{route('tax-payer.form')}}">
                    <select id="TformChange" name="select" id="userType" class="form-control" name="type">
                         <option value="">নির্বাচন করুন</option>
                        <option value="1">সাধারণ হোল্ডিং</option>
                        <option value="2">বানিজিক হোল্ডিং</option>
                    </select>
        </form>
    </div>
</div>
@stop
@push('script')
    <script>
    $("#TformChange").change(function(){
     $("#userForm").submit();
    });
    </script>
@endpush
