@extends('backend.layouts.master')

@section('title','নতুন করদাতা')
@section('master_content')
<div class="container">
    <h3 class="text-center">নতুন করদাতা তৈরি ফর্ম</h3>
    <hr>
       <div class="form-group">
                <form id="userForm" method="get" action="{{route('tax-payer.form')}}">
                    <select id="TformChange" name="select" id="userType" class="form-control" name="type">
                         <option value="">নির্বাচন করুন</option>
                        <option value="1">সাধারণ হোল্ডিং</option>
                        <option value="2" selected>বানিজিক হোল্ডিং</option>
                    </select>
        </form>
    </div>
</div>

<div class="container">
    <h3 class="text-center">বানিজিক হোল্ডিং</h3>
    <div class="card">
        <div class="card-body">
            <form action="">
                <div class="row">
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                           <label for="">ট্যাক্স নির্ধারন পদ্ধতি: </label>
                                <select name="tax_way" id="tax_way" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    <option value="pesha_vittik">পেশা ভিত্তিক </option>
                                    <option value="barshik_mullayon">বার্ষিক মূল্যায়ন</option>
                                </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                                <label for="">ওয়ার্ড নং নির্বাচন করুন : </label>
                                <select name="ward_id" id="ward_id" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    <option value="">{{ englishToBanglaNumber(1) }}</option>
                                    <option value="">{{ englishToBanglaNumber(2) }}</option>
                                    <option value="">{{ englishToBanglaNumber(3) }}</option>
                                    <option value="">{{ englishToBanglaNumber(4) }}</option>
                                </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">হোল্ডিং নং</label>
                            <input type="text" class="form-control" placeholder="হোল্ডিং নং" name="holding_no">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">বাজার নাম</label>
                            <input type="text" class="form-control" placeholder="বাজার নাম" name="bazar">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">পোস্টের নাম</label>
                            <input type="text" class="form-control" placeholder="পোস্টের নাম" name="post_name">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">পোস্ট কোড</label>
                            <input type="text" class="form-control" placeholder="পোস্ট কোড" name="post_code">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">ধর্ম</label>
                            <input type="text" class="form-control" placeholder="ধর্ম" name="religion">
                        </div>
                    </div>

                    <div class="col-12 col-md-3">
                        <div class="form-group">
                           <label for="">লিঙ্গ : </label>
                                <select name="gender" id="gender" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    <option value="male">পুরুষ</option>
                                    <option value="female">মহিলা</option>
                                    <option value="others">অন্যান্য</option>
                                </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">কর প্রদানকারীর নাম</label>
                            <input type="text" class="form-control" placeholder="কর প্রদানকারীর নাম" name="name">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">কর প্রদানকারীর পিতার নাম</label>
                            <input type="text" class="form-control" placeholder="কর প্রদানকারীর পিতার নাম" name="fathers_name">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">এন আই ডি নং</label>
                            <input type="text" class="form-control" placeholder="এন আই ডি নং" name="nid">
                        </div>
                    </div>
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">মোবাইল</label>
                            <input type="text" class="form-control" placeholder="মোবাইল নাম্বার" name="mobile">
                        </div>
                    </div>
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">ইমেল</label>
                            <input type="email" class="form-control" placeholder="ইমেল" name="email">
                        </div>
                    </div>
                      <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">মূল্য</label>
                            <input type="value" class="form-control" placeholder="মূল্য" name="value">
                        </div>
                    </div>
                      <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">ইমেল</label>
                            <input type="email" class="form-control" placeholder="ইমেল" name="email">
                        </div>
                    </div>
                </div>
                <!-- dynamic filed add -->
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="">রুম: </label>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="multipleRoomTable">
                                    <tr>
                                        <th>#</th>
                                        <th>রুমের ধরণ</th>
                                        <th>ভাড়া</th>
                                        <th>স্ব ব্যবহার</th>
                                        <th>মাসিক ভাড়া</th>
                                        <th>বার্ষিক মান</th>
                                        <th>বার্ষিক কর</th>
                                        <th><button id="addNewRoomBtn" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i></button></th>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="width: 150px">
                                            <select name="room_conditions" id="room_conditions" class="form-control" required>
                                            <option value="">নির্বাচন করুন</option>
                                            <option value="male">পাকা</option>
                                            <option value="female">আধা পাকা</option>
                                            <option value="others">মাটি</option>
                                        </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="vara" placeholder="ভাড়া">
                                        </td>
                                        <td><input type="text" class="form-control" placeholder=""></td>
                                        <td><input type="text" class="form-control" placeholder="মাসিক ভাড়া"></td>
                                        <td><input type="text" class="form-control" placeholder="বার্ষিক মান"></td>
                                        <td><input type="text" class="form-control" placeholder="বার্ষিক কর"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row">
                       <div class="col-12">
                            <div class="form-group">
                            <button class="btn btn-block btn-secondary">Submit</button>
                        </div>
                       </div>
                    </div>
            </form>
        </div>
    </div>
</div>
@stop
@push('script')
    <script>
    $("#TformChange").change(function(){
     $("#userForm").submit();
    });
    </script>

    <script>
        //dynamic room add
        let counter = 0
        $('#addNewRoomBtn').click((e) => {
            e.preventDefault()
            let html = `
             <tr id="row${counter}">
                                        <td></td>
                                        <td style="width: 150px">
                                            <select name="room_conditions" class="form-control" required>
                                            <option value="">নির্বাচন করুন</option>
                                            <option value="male">পাকা</option>
                                            <option value="female">আধা পাকা</option>
                                            <option value="others">মাটি</option>
                                        </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="vara" placeholder="ভাড়া">
                                        </td>
                                        <td><input type="text" class="form-control" placeholder=""></td>
                                        <td><input type="text" class="form-control" placeholder="মাসিক ভাড়া"></td>
                                        <td><input type="text" class="form-control" placeholder="বার্ষিক মান"></td>
                                        <td><input type="text" class="form-control" placeholder="বার্ষিক কর"></td>
                                        <td><button class="btn btn-sm btn-danger removeDynamicRow" id="${counter}"><i class="fa fa-minus-circle"></i></button></td>
                                    </tr>
                                    ${counter++}
            `
            $('#multipleRoomTable').append(html)
        })
        //remove dynamic row
        $('body').on('click','.removeDynamicRow',(e) => {
            e.preventDefault()
           var id = $('.removeDynamicRow').attr('id');
           //console.log(id);
            $('#row'+id).remove();
        })
    </script>
@endpush
