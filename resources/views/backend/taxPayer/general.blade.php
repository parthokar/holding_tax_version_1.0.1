@extends('backend.layouts.master')

@section('title','নতুন করদাতা')
@section('master_content')
<div class="container">
    <h3 class="text-center">নতুন করদাতা তৈরি ফর্ম</h3>
    <hr>
       <div class="form-group">
                <form id="userForm" method="get" action="{{route('tax-payer.form')}}">
                    <select id="TformChange" name="select" id="userType" class="form-control" name="type">
                         <option value="">নির্বাচন করুন</option>
                        <option value="1" selected>সাধারণ হোল্ডিং</option>
                     <option value="2">বানিজিক হোল্ডিং</option>
            </select>
        </form>
    </div>
</div>
<hr>
<div class="container">
    <h3 class="text-center">সাধারণ হোল্ডিং</h3>
    <div class="card">
        <div class="card-body">
            <form action="{{ route('tax-payer.form') }}" id="taxPayerForm" method="POST">
                @csrf
                <div class="row">
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                           <label for="">ট্যাক্স নির্ধারন পদ্ধতি: </label>
                                <select name="tax_way" id="tax_way" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    <option value="pesha_vittik">পেশা ভিত্তিক </option>
                                    <option value="barshik_mullayon">বার্ষিক মূল্যায়ন</option>
                                </select>
                            <span class="text-danger">{{($errors->has('tax_way'))? ($errors->first('tax_way')) : ''}}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                                <label for="">ওয়ার্ড নং করুন : </label>
                           <select name="ward_id" id="ward_id" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    @forelse ($wards as $ward)
                                        <option value="{{ $ward->id }}">{{ englishToBanglaNumber($ward->word_name )}}</option>
                                    @empty
                                        <option value="">খালি</option>
                                    @endforelse
                            </select>
                                 <span class="text-danger">{{($errors->has('ward_id'))? ($errors->first('ward_id')) : ''}}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">হোল্ডিং নং</label>
                            <input type="text" class="form-control" placeholder="হোল্ডিং নং" name="holding_no" value="{{ old('holding_no') }}">
                             <span class="text-danger">{{($errors->has('holding_no'))? ($errors->first('holding_no')) : ''}}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">গ্রামের নাম</label>
                            <input type="text" class="form-control" placeholder="গ্রামের নাম" name="village" value="{{ old('village') }}">
                            <span class="text-danger">{{($errors->has('village'))? ($errors->first('village')) : ''}}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">পোস্টের নাম</label>
                            <input type="text" class="form-control" placeholder="পোস্টের নাম" name="post_name" value="{{ old('post_name') }}">
                             <span class="text-danger">{{($errors->has('post_name'))? ($errors->first('post_name')) : ''}}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">পোস্ট কোড</label>
                            <input type="text" class="form-control" placeholder="পোস্ট কোড" name="post_code" value="{{ old('post_code') }}">
                            <span class="text-danger">{{($errors->has('post_code'))? ($errors->first('post_code')) : ''}}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">ধর্ম</label>
                            <input type="text" class="form-control" placeholder="ধর্ম" name="religion" value="{{ old('religion') }}">
                            <span class="text-danger">{{($errors->has('religion'))? ($errors->first('religion')) : ''}}</span>
                        </div>
                    </div>

                    <div class="col-12 col-md-3">
                        <div class="form-group">
                           <label for="">লিঙ্গ : </label>
                                <select name="gender" id="gender" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    <option value="male">পুরুষ</option>
                                    <option value="female">মহিলা</option>
                                    <option value="others">অন্যান্য</option>
                                </select>
                        </div>
                    </div>
                      <div class="col-12 col-md-3" id="marriedConditions">
                        <div class="form-group">
                           <label for="">বিবাহিত অবস্থা: </label>
                                <select name="is_married" id="is_married" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    <option value="1">হ্যাঁ</option>
                                    <option value="0">না</option>
                                </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">কর প্রদানকারীর নাম</label>
                            <input type="text" class="form-control" placeholder="কর প্রদানকারীর নাম" name="name" value="{{ old('name') }}">
                            <span class="text-danger">{{($errors->has('name'))? ($errors->first('name')) : ''}}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-3" id="fatherName">
                        <div class="form-group" >
                            <label for="">কর প্রদানকারীর পিতার নাম</label>
                            <input type="text" class="form-control" placeholder="কর প্রদানকারীর পিতার নাম" name="fathers_name">
                        </div>
                    </div>
                     <div class="col-12 col-md-3" id="husbandName">
                        <div class="form-group" >
                            <label for="">কর প্রদানকারীর স্বামীর নাম</label>
                            <input type="text" class="form-control" placeholder="কর প্রদানকারীর স্বামীর নাম" name="husband_name">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">এন আই ডি নং</label>
                            <input type="text" class="form-control" placeholder="এন আই ডি নং" name="nid" value="{{ old('nid') }}">
                             <span class="text-danger">{{($errors->has('nid'))? ($errors->first('nid')) : ''}}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">বাসা নং</label>
                            <input type="text" class="form-control" placeholder="বাসা নং" name="home_no" value="{{ old('home_no') }}">
                             <span class="text-danger">{{($errors->has('home_no'))? ($errors->first('home_no')) : ''}}</span>
                        </div>
                    </div>
                     <div class="col-12 col-md-3">
                         <div class="form-group">
                           <label for="">বাড়ির অবস্থা : </label>
                                <select name="home_conditions" id="home_conditions" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    @forelse ($houses as $home)
                                        <option value="{{ $home->id }}">{{ $home->house_name }}</option>
                                    @empty
                                        <option value="">খালি</option>
                                    @endforelse
                                </select>
                        </div>
                    </div>
                      <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">কক্ষ নম্বর</label>
                            <select name="room_no" id="room_no" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    @forelse ($rooms as $room)
                                        <option value="{{ $room->id }}">{{ englishToBanglaNumber($room->room_name )}}</option>
                                    @empty
                                        <option value="">খালি</option>
                                    @endforelse
                                </select>
                            <span class="text-danger">{{($errors->has('room_no'))? ($errors->first('room_no')) : ''}}</span>
                        </div>
                    </div>
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                           <label for="">কাজের ধরণ: </label>
                            <select name="job" id="job" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    @forelse ($jobs as $job)
                                        <option value="{{ $job->id }}">{{ $job->job_type }}</option>
                                    @empty
                                        <option value="">খালি</option>
                                    @endforelse
                            </select>
                        </div>
                    </div>
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">মোবাইল</label>
                            <input type="text" class="form-control" placeholder="মোবাইল নাম্বার" name="mobile" value="{{ old('mobile') }}">
                            <span class="text-danger">{{($errors->has('mobile'))? ($errors->first('mobile')) : ''}}</span>
                        </div>
                    </div>
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">ইমেল</label>
                            <input type="email" class="form-control" placeholder="ইমেল" name="email" value="{{ old('email') }}">
                            <span class="text-danger">{{($errors->has('email'))? ($errors->first('email')) : ''}}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">ভূমির পরিমাণ</label>
                            <input type="value" class="form-control" placeholder="ভূমির পরিমাণ" name="vumir_poriman" value="{{ old('vumir_poriman') }}">
                            <span class="text-danger">{{($errors->has('vumir_poriman'))? ($errors->first('vumir_poriman')) : ''}}</span>
                        </div>
                    </div>
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">মোট ভূমির পরিমাণ (শতক )</label>
                            <input type="value" class="form-control" placeholder="মোট ভূমির পরিমাণ (শতক )" name="vumir_poriman_shotok">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">আবাদি ভূমির পরিমাণ (শতক )</label>
                            <input type="value" class="form-control" placeholder="আবাদি ভূমির পরিমাণ (শতক )" name="abadi_poriman">
                        </div>
                    </div>
                       <div class="col-12 col-md-3">
                        <div class="form-group">
                           <label for="">স্যানিটেশন: </label>
                                <select name="sanitation" id="sanitation" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    <option value="paka">পাকা</option>
                                    <option value="adapaka">আধা পাকা</option>
                                    <option value="mati">মাটি</option>
                                </select>
                        </div>
                    </div>
                      <div class="col-12 col-md-3">
                        <div class="form-group">
                           <label for="">সম্পত্তি: </label>
                                <select name="sompoti" id="sompoti" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    <option value="nij">নিজ</option>
                                    <option value="vara">ভাড়া </option>
                                </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">পরিবারের সদস্য</label>
                            <input type="text" class="form-control" id="family_member" placeholder="পরিবারের সদস্য" name="family_member">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">পুরুষ</label>
                            <input type="text" class="form-control" placeholder="পুরুষ" name="male" id="male">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">মহিলা</label>
                            <input type="text" class="form-control" placeholder="মহিলা "name="female" id="female">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                           <label for="">নলকূপ: </label>
                                <select name="has_tubewell" id="tubewell" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    <option value="1">হ্যাঁ</option>
                                    <option value="0">না</option>
                                </select>
                        </div>
                    </div>
                     <div class="col-12 col-md-3">
                        <div class="form-group">
                           <label for="">বিদ্যুৎ: </label>
                                <select name="has_electricty" id="electricty" class="form-control" required>
                                    <option value="">নির্বাচন করুন</option>
                                    <option value="1">হ্যাঁ</option>
                                    <option value="0">না</option>
                                </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">মূল্য</label>
                            <input type="text" class="form-control" placeholder="মূল্য" name="tax_value" id="tax_value">
                        </div>
                    </div>

                     <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">সর্বশেষ ট্যাক্স পেইড ইয়ার</label>
                            <input type="text" class="form-control" placeholder="সর্বশেষ ট্যাক্স পেইড ইয়ার" name="last_tax_year">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="">প্রতি বছর কর</label>
                            <input type="text" class="form-control" placeholder="প্রতি বছর কর" name="tax_per_year" id="tax_per_year">
                        </div>
                    </div>

                </div>
                 <div class="row">
                       <div class="col-12">
                            <div class="form-group">
                            <button class="btn btn-block btn-secondary" type="submit">Submit</button>
                        </div>
                       </div>
                    </div>
            </form>
        </div>
    </div>
</div>
@stop
@push('script')
    <script>
    //dymanic form change
    $("#TformChange").change(function(){
     $("#userForm").submit();
    });
    </script>

    <script>
    //dynamic input filed
    let marriedConditions = $('#marriedConditions')
    let husbandName = $('#husbandName')
    let fatherName = $('#fatherName')
    marriedConditions.hide()
    husbandName.hide()

    //change gender conditions
    $('body').on('change','#gender',(e) => {
        e.preventDefault();
        let gender = $('#gender').val()
        if(gender === 'female'){
            marriedConditions.show()
        }else{
             marriedConditions.hide()
        }
    })
     //change married conditions
    $('body').on('change','#is_married',(e) => {
        e.preventDefault();
        let married = $('#is_married').val()
        //console.log(married);
        if(married === '1'){
            husbandName.show()
            fatherName.hide()
        }else if(married === '0'){
            husbandName.hide()
            fatherName.show()
        }
    })
    //family member value
    let family_member = $('#family_member')
    let male = $('#male')
    let female = $('#female')
    let total = 0
    let maleMember = 0
    $('#family_member').on('blur',(e) => {

        total = $('#family_member').val()
    })
    $('#male').on('keyup',(e) => {
        maleMember = $('#male').val()
        let diff = total - maleMember
        female.val(diff)
    })

    //disable option
    $('#tax_way').on('change',(e) => {
        e.preventDefault()
        $('#tax_value').val('')
        $('#tax_per_year').val('')
        let way = $('#tax_way').val()
        if(way === 'pesha_vittik'){
            $('#tax_value').prop('readonly',true)
            $('#tax_per_year').attr('readonly',true)
        }else if(way === 'barshik_mullayon'){
            $('#tax_value').attr('readonly',false)
            $('#tax_per_year').attr('readonly',false)
        }else if(way === ''){
            alert('Select an Option!')
            $('#tax_value').attr('readonly',false)
            $('#tax_per_year').attr('readonly',false)
        }
    })

    </script>
    {{-- Validation --}}
    @includeIf('backend.includes.validation.generalHoldingTaxpayer')

    {{-- Calculation --}}
    <script>
        $('body').on('change','#job',(e)=>{
            e.preventDefault()
            let job = $('#job').val()
            let room = $('#room_no').val()
            let house = $('#home_conditions').val()
            let tax_way = $('#tax_way').val()
            let data = {
               house_id : house ,room_id : room,job_id : job
            }
            if(tax_way === 'pesha_vittik'){
                axios.post('{{ route('general-holding-tax') }}',data)
                .then((res)=>{
                    //console.log(res.data);
                    $('#tax_value').val(res.data.tax_amount)
                    //console.log(typeof res.data.tax_percent);
                    $('#tax_per_year').val((Number(res.data.tax_amount*res.data.tax_percent))/100)
                    //console.log(res.data);
                })
                .catch((err) =>{
                    console.log(err);
                })

            }
            //console.log(job + room + house);
        })
    </script>
@endpush
