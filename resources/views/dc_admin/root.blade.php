<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title', config('app.name'))</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('backend/plugins/toastr/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    @yield('styles')
    <link rel="stylesheet" href="{{ asset('backend/dist/css/adminlte.min.css')}}">
</head>
<body id="app" class="hold-transition sidebar-mini sidebar-collapse layout-fixed layout-navbar-fixed layout-footer-fixed">
<form id="AdminDialogSystemOut" class="modal fade" action="" method="POST"> @csrf
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center pt-5">
                <h3>Are you sure you want to logout now? </h3>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-success flex-grow-1" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger flex-grow-1"> Ok </button>
            </div>
        </div>
    </div>
</form>
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button">
                    <i class="fas fa-bars"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            @include('dc_admin.notifications')
            <li class="nav-item">
                <a data-toggle="modal" data-target="#AdminDialogSystemOut" class="nav-link" role="button"
                   href="javascript:void(0);">
                    <i class="fas fa-power-off"></i>
                </a>
            </li>
        </ul>
    </nav>
    @include('dc_admin.sidebar')
    <main class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                @yield('body')
            </div>
        </section>
    </main>
    <footer class="main-footer">
        <b>&copy; {{date('Y')}} <a href="{{url('/')}}">{{config('app.name')}}</a>.</b> @lang('All rights reserved.')
    </footer>
</div>
<script src="{{ asset('backend/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('backend/plugins/toastr/toastr.min.js')}}"></script>
<script src="{{ asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('backend/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
@yield('scripts')
<script src="{{ asset('backend/dist/js/adminlte.min.js')}}"></script>
</body>
</html>
