@extends('dc_admin.auth')
@section('body')
    <div class="login-page">
        <div class="login-box">
            <div class="login-logo">
            </div>
            <div class="card">
                <div class="card-body login-card-body">
                    <p class="login-box-msg">Sign in to start your session</p>
                    @if(Route::has('dc_admin.login'))
                    <form action="{{route('dc_admin.login')}}" method="post">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <span class="text-danger">{{($errors->has('email'))? ($errors->first('email')) : ''}}</span>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control" placeholder="Password" name="password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>
                        <span class="text-danger">{{($errors->has('password'))? ($errors->first('password')) : ''}}</span>
                        <div class="row">
                            <div class="col-8">
                                <div class="icheck-primary">
                                    <input type="checkbox" id="remember">
                                    <label for="remember">
                                        Remember Me
                                    </label>
                                </div>
                            </div>
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                            </div>
                        </div>
                    </form>
                    @endif
                    <p class="mb-1">
                        <a href="#">I forgot my password</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
